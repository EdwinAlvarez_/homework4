/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme;

/**
 *
 * @author edwinalvarez
 */
public enum PropertyType {
    
    // SUB REGION CONSTANTS
    
    /**
     * 
     */
    OK,
    
    /**
     * 
     */
    LEADER_NAME_LABEL,
    
    /**
     * 
     */
    CAPITAL_NAME_LABEL,
    
    /**
     * 
     */
    REGION_NAME_LABEL,
    
    /**
     * 
     */
    NO_FLAG_IMAGE_MESSAGE,
    
    /**
     * 
     */
    NO_LEADER_IMAGE_MESSAGE,
    
    /**
     * 
     */
    WORKSPACE_PATH_CSS,
    
    /**
     * 
     */
    SUBREGION_DIALOG_TITLE,
    
    /**
     * 
     */
    NO_IMAGE,
    
    // BUTTON ICONS
    
    /**
     * 
     */
    ADD_ICON,
    
    /**
     * 
     */
    REMOVE_ICON,
    
    /**
     * 
     */
    MOVE_UP_ICON,
    
    /**
     * 
     */
    MOVE_DOWN_ICON,
    
    /**
     * 
     */
    PLAY_AUDIO_ICON,
    
    /**
     * 
     */
    STOP_AUDIO_ICON,
    
    /**
     * 
     */
    PREV_ICON,
    
    /**
     * 
     */
    NEXT_ICON,
    
    /**
     * 
     */
    NEW_IMAGE_ICON,
    
    /**
     * 
     */
    CHANGE_NAME_ICON,
    
    /**
     * 
     */
    CHANGE_COLORS_ICON,
    
    /**
     * 
     */
    REASSIGN_ICON,
    
    /**
     * 
     */
    LOAD_ICON_W,
    
    /**
     * 
     */
    DIMENSIONS_ICON,
    
    /**
     * 
     */
    FOLDER_ICON,
    
    /**
     * 
     */
    FILE_ICON,
    
    /**
     * 
     */
    
    // BUTTON TOOLTIPS
    
    ADD_IMAGE_TOOLTIP,
    
    /**
     * 
     */
    REMOVE_IMAGE_TOOLTIP,
    
    /**
     * 
     */
    MOVE_LEFT_TOOLTIP,
    
    /**
     * 
     */
    MOVE_RIGHT_TOOLTIP,
    
    /**
     * 
     */
    PLAY_SONG_TOOLTIP,
    
    /**
     * 
     */
    STOP_SONG_TOOLTIP,
    
    /**
     * 
     */
    NEW_IMAGE_TOOLTIP,
    
    /**
     * 
     */
    CHANGE_COLORS_TOOLTIP,
    
    /**
     * 
     */
    CHANGE_NAME_TOOLTIP,
    
    /**
     * 
     */
    REASSIGN_TOOLTIP,
    
    /**
     * 
     */
    LOAD_TOOLTIP_W,
    
    /**
     * 
     */
    DIMENSIONS_TOOLTIP,
    
    // EDIT TOOLBAR
    
    /**
     * 
     */
    COLUMN_LEADER,
    
    /**
     * 
     */
    COLUMN_CAPITAL,
    
    /**
     * 
     */
    COLUMN_SUBREGION,
    
    /**
     * 
     */
    RENAME_MAP_TEXT,
    
    /**
     * 
     */
    REASSIGN_COLORS_TEXT,
    
    /**
     * 
     */
    BORDER_COLOR_LABEL,
    
    /**
     * 
     */
    BANCKGROUND_COLOR_LABEL,
    
    /**
     * 
     */
    BORDER_WIDTH_LABEL,
    
    /**
     * 
     */
    ZOOM_LABEL,
    
    /**
     * 
     */
    BORDER_THICKNESS_LABEL,
    
    // OTHER TEXT STUFF
    
    /**
     * 
     */
    LOADING_MESSAGE,
    
    /**
     * 
     */
    NEW_REGION_NAME_MESSAGE,
    
    /**
     * 
     */
    REGION_NAME_DIALOG_TITLE,
    
    /**
     * 
     */
    NEW_IMAGE_LABEL,
    
    /**
     * 
     */
    NEW_IMAGE_TITLE,
    
    /**
     * 
     */
    NEW_DIMESIONS_ERROR_MESSAGE,
    
    /**
     * 
     */
    
    NEW_DIMESIONS_ERROR_TITLE,
    
    /**
     * 
     */
    ANTHEM_NOT_FOUND_MESSAGE,
    
    /**
     * 
     */
    ANTHEM_NOT_FOUND_TITLE,
    
    // NEW MAP TEXT
    
    /**
     * 
     */
    NEW_REGION_TITLE,
    
    /**
     * 
     */
    NEW_REGION_NAME,
    
    /**
     * 
     */
    NEW_REGION_PARENT_DIR,
    
    /**
     * 
     */
    NEW_REGION_COORDINATE_DIR,
    
    /**
     * 
     */
    BUTTON_CONTINUE,
    
    /**
     * 
     */
    BUTTON_BROWSE_COMPUTER,
    
    /**
     * 
     */
    NEW_WIDTH_LABEL,
    
    /**
     * 
     */
    NEW_HEIGHT_LABEL,
    
    /**
     * 
     */
    NEW_DIMESIONS_TITLE,
    
    /**
     * 
     */
    WORK_FILE_EXT,
    
    // PROGRESS BAR VALUES
    
    /**
     * 
     */
    PROGRESS_BAR_LABEL,
    
    /**
     * 
     */
    PROGRESS_BAR_TITLE
}
