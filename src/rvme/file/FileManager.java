/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.stage.DirectoryChooser;
import javax.imageio.ImageIO;
import rvme.data.DataManager;
import rvme.gui.NewMapDialog;
import rvme.gui.Workspace;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import properties_manager.PropertiesManager;
import rvme.controller.EditorController;
import rvme.data.SubRegion;
import static saf.settings.AppPropertyType.SAVE_ERROR_MESSAGE;
import static saf.settings.AppPropertyType.SAVE_ERROR_TITLE;
import saf.ui.AppMessageDialogSingleton;

/**
 *
 * @author edwinalvarez
 */
public class FileManager implements AppFileComponent{
    // DATA JSON STATIC VARIABLES
    static final String PARENT_DIRECTORY = "parent_directory";
    static final String REGION_NAME = "region_name";
    static final String BORDER_WIDTH = "border_width";
    static final String BORDER_COLOR = "border_color";
    static final String BACKGROUND_COLOR = "background_color";
    static final String MAP_COORDINATE_FILE = "map_coordinate_file";
    static final String IMAGES = "images";
    static final String SUBREGIONS = "subregions";
    static final String MAP_X_DISPLACEMENT = "map_x_displacement";
    static final String MAP_Y_DISPLACEMENT = "map_y_displacement";
    static final String DATA_MANAGER_NUM_OF_SUBREGIONS = "num_of_subregions";
    static final String MAP_WIDTH_DIMENSION = "map_width_dimension";
    static final String MAP_HEIGHT_DIMENSION = "map_height_dimension";
    static final String ZOOM_SCALE = "zoom_scale";
    static final String IMAGE_FILE_PATH = "image_file_path";
    static final String IMAGE_POX_X = "pos_x";
    static final String IMAGE_POX_Y = "pos_y";
    static final String SUBREGION_NAME = "name";
    static final String SUBREGION_CAPITAL = "capital";
    static final String SUBREGION_LEADER = "leader";
    static final String SUBREGION_RED = "red";
    static final String SUBREGION_GREEN = "green";
    static final String SUBREGION_BLUE = "blue";
    static final String SUBREGION_NUM_OF_POLYGONs = "num_of_polygons";
    
    // MAP JSON STATIC VARIABLES
    static final String TOTAL_SUBREGIONS = "NUMBER_OF_SUBREGIONS";
    static final String SUBREGIONS_ARRAY = "SUBREGIONS";
    static final String SUBREGIONS_POLYGON_ARRAY = "SUBREGION_POLYGONS";
    static final String TOTAL_SUBREGION_POLYGONS = "NUMBER_OF_SUBREGION_POLYGONS";
    static final String X = "X";
    static final String Y = "Y";
    static final double DEFAULT_WIDTH = 802.0;
    static final double DEFAULT_HEIGHT = 536.0;
    
    // EXPORT VARIABLES
    static final String SUBREGIONS_HAVE_CAPITAL = "subregions_have_capitals";
    static final String SUBREGIONS_HAVE_FLAGS = "subregions_have_flags";
    static final String SUBREGIONS_HAVE_LEADERS = "subregions_have_leaders";
    
    public static String currentWorkFile;
    

    Workspace workspace;
    DataManager dataManager;

    
    
    public FileManager(){
        
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        
        DataManager dataManager = (DataManager)data;
        
        // SAVING SUBREGION DATA
        JsonArrayBuilder subRegionArrayBuilder = Json.createArrayBuilder();
        ObservableList<SubRegion> subRegions = dataManager.getSubRegionList();
        for(SubRegion subRegion: subRegions){
            JsonObject subRegionsObject = Json.createObjectBuilder()
                    .add(SUBREGION_NAME, subRegion.getName())
                    .add(SUBREGION_CAPITAL, subRegion.getCapital())
                    .add(SUBREGION_LEADER, subRegion.getLeader())
                    .add(SUBREGION_RED, (int)(subRegion.getColor().getRed() * 254))
                    .add(SUBREGION_GREEN, (int)(subRegion.getColor().getGreen() * 254))
                    .add(SUBREGION_BLUE, (int) (subRegion.getColor().getBlue() * 254))
                    .add(SUBREGION_NUM_OF_POLYGONs, subRegion.getNumOfPolygons()).build();
            
            subRegionArrayBuilder.add(subRegionsObject);      
        }
        JsonArray subRegionJsonArray = subRegionArrayBuilder.build();
        
        
        // NOW WE SAVE THE IMAGES AND THEIR DATA
        JsonArrayBuilder imagesArrayBuilder = Json.createArrayBuilder();
        ArrayList<String> imagePaths = dataManager.getImagePaths();
        ArrayList<Double[]> imageDisplacements = dataManager.getImageDisplacements();
        for(int i = 0 ; i < imagePaths.size(); i++){
            JsonObject imageObject = Json.createObjectBuilder()
                    .add(IMAGE_FILE_PATH, imagePaths.get(i))
                    .add(IMAGE_POX_X, imageDisplacements.get(i)[0])
                    .add(IMAGE_POX_Y, imageDisplacements.get(i)[1]).build();
            
            imagesArrayBuilder.add(imageObject);
        }
        
        JsonArray imagesJsonArray = imagesArrayBuilder.build();
        
        // NOW WE PUT IT ALL TOGETHER
        JsonObject dataManagerJSON = Json.createObjectBuilder()
                .add(PARENT_DIRECTORY, dataManager.getParentDirectory())
                .add(REGION_NAME, dataManager.getRegionName())
                .add(BORDER_WIDTH, dataManager.getBorderWidth())
                .add(BORDER_COLOR, dataManager.getBorderColorString())
                .add(BACKGROUND_COLOR, dataManager.getBackgroundColorString())
                .add(MAP_COORDINATE_FILE, dataManager.getCoordinateFilePath())
                .add(MAP_X_DISPLACEMENT, dataManager.getMapDisplacements()[0])
                .add(MAP_Y_DISPLACEMENT, dataManager.getMapDisplacements()[1])
                .add(DATA_MANAGER_NUM_OF_SUBREGIONS, dataManager.getNumOfSubregions())
                .add(MAP_WIDTH_DIMENSION, dataManager.getMapWidth())
                .add(MAP_HEIGHT_DIMENSION, dataManager.getMapHeight())
                .add(ZOOM_SCALE, dataManager.getZoomScale())
                .add(IMAGES, imagesJsonArray)
                .add(SUBREGIONS, subRegionJsonArray).build();
        
        
        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSON);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSON);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        // INIT THE CURRENT WORK FILE
        currentWorkFile = filePath;
        
        dataManager = (DataManager) data;
        JsonObject json = loadJSONFile(filePath);

       // LOADING MAP DATA
        dataManager.reset();
        loadMapData(json, dataManager);
        
        // GETTING THE COORDINATE FILE DATA PATH
        String coordinateDir = getDataAsString(json, MAP_COORDINATE_FILE);
        
        // LOADING THE SUBREGIONS
        loadSubRegions(dataManager, coordinateDir);
        
        // LOADING SUB REGION INFORMATION
        JsonArray subRegionInfoArray = json.getJsonArray(SUBREGIONS);
        loadSubRegionInfo(subRegionInfoArray, dataManager);
        
        // LOADING MAP IMAGES
        JsonArray imageJsonArray = json.getJsonArray(IMAGES);
        loadMapImages(imageJsonArray, dataManager);
    }
    
    private double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    private String getDataAsString(JsonObject json, String dataName){
        JsonValue value = json.get(dataName);
        JsonString string = (JsonString)value;
        return string.getString();
    }
    
    private int getDataAsInt(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber)value;
        return number.bigIntegerValue().intValue();
    }
    
    private boolean getDataAsBoolean(JsonObject json, String dataName) {
        return (json.getInt(dataName) == 1);
    }
    
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    private void loadMapData(JsonObject json, DataManager dataManager) throws IOException{
        dataManager.setParentDirectory(getDataAsString(json, PARENT_DIRECTORY));
        dataManager.setRegionName(getDataAsString(json, REGION_NAME));
        dataManager.setBorderWidth(getDataAsDouble(json, BORDER_WIDTH));
        dataManager.setBorderColorString(getDataAsString(json, BORDER_COLOR));
        dataManager.setBackgroundColorString(getDataAsString(json, BACKGROUND_COLOR));
        dataManager.setCoordinateFilePath(getDataAsString(json, MAP_COORDINATE_FILE));
        dataManager.setNumOfSubregions(getDataAsInt(json, DATA_MANAGER_NUM_OF_SUBREGIONS));
        dataManager.setMapWidth(getDataAsDouble(json, MAP_WIDTH_DIMENSION));
        dataManager.setMapHeight(getDataAsDouble(json, MAP_HEIGHT_DIMENSION));
        dataManager.setZoomScale(getDataAsDouble(json, ZOOM_SCALE));
        dataManager.getMapDisplacements()[0] = getDataAsDouble(json, MAP_X_DISPLACEMENT);
        dataManager.getMapDisplacements()[1] = getDataAsDouble(json, MAP_Y_DISPLACEMENT);
    }
    
    public void loadSubRegions(DataManager dataManager, String filePath) throws IOException {
        int numOfSubPolygons, numOfSubregions;
        
        // CLEAR OLD DATA OUT
        //workspace = (Workspace) dataManager.getApp().getWorkspaceComponent();
        
        // LOAD THE JSON FILE
        JsonObject json = loadJSONFile(filePath);
        numOfSubregions = getDataAsInt(json, TOTAL_SUBREGIONS);
        dataManager.setNumOfSubregions(numOfSubregions);
        
        // GETTING THE CORDINATES FROM THE JSON FILES
        // Fist we access the array of arrays
        JsonArray jsonSubregionArray = json.getJsonArray(SUBREGIONS_ARRAY);
        JsonArray jsonSubregionPolygonArray;
        ArrayList<Double> list = new ArrayList<>();
        
        for(double i = 0; i < jsonSubregionArray.size(); i++){
            // Now we get the subreagion polygons array
            // current subregion being created
            SubRegion subRegion = new SubRegion();
            //progressPerc.setValue(i/jsonSubregionArray.size());
            double perc = i/jsonSubregionArray.size();
            //progressBar.setProgress(perc);
            
            JsonObject jsonSubregionObject = jsonSubregionArray.getJsonObject((int)i);
            numOfSubPolygons = getDataAsInt(jsonSubregionObject, TOTAL_SUBREGION_POLYGONS);
            subRegion.setNumOfPolygons(numOfSubPolygons);
            jsonSubregionPolygonArray = jsonSubregionObject.getJsonArray(SUBREGIONS_POLYGON_ARRAY);
            
            for(int j = 0; j < jsonSubregionPolygonArray.size(); j++){
                // accesing each subregion polygon
                JsonArray jsonCoordinates = jsonSubregionPolygonArray.getJsonArray(j);
                
                for(int k = 0; k < jsonCoordinates.size(); k++){
                    list.add(getDataAsDouble(jsonCoordinates.getJsonObject(k), X));
                    list.add(getDataAsDouble(jsonCoordinates.getJsonObject(k), Y));
                }
                
                subRegion.addPolygon(list, DEFAULT_HEIGHT, DEFAULT_WIDTH);
                list.clear();
            }
            dataManager.addSubRegion(subRegion);
            
        }
        
    }
    
    private void loadSubRegionInfo(JsonArray subRegionArray, DataManager dataManager){
        
        for(int i = 0; i < subRegionArray.size(); i++){
            
            JsonObject currentJsonSubRegion = subRegionArray.getJsonObject(i);
            SubRegion currentSubRegion = dataManager.getSubRegionList().get(i);
            
            currentSubRegion.setName(getDataAsString(currentJsonSubRegion, SUBREGION_NAME));
            currentSubRegion.setCapital(getDataAsString(currentJsonSubRegion, SUBREGION_CAPITAL));
            currentSubRegion.setLeader(getDataAsString(currentJsonSubRegion, SUBREGION_LEADER));
            
            int r = getDataAsInt(currentJsonSubRegion, SUBREGION_RED);
            int g = getDataAsInt(currentJsonSubRegion, SUBREGION_GREEN);
            int b = getDataAsInt(currentJsonSubRegion, SUBREGION_BLUE);
            
            currentSubRegion.setColor(r, g, b);
            currentSubRegion.setNumOfPolygons(getDataAsInt(currentJsonSubRegion, SUBREGION_NUM_OF_POLYGONs));
        }
    }
    
    private void loadMapImages(JsonArray imageJsonArray, DataManager dataManager){
        
        for(int i = 0; i < imageJsonArray.size(); i++){
            JsonObject currentJsonImage = imageJsonArray.getJsonObject(i);
            dataManager.getImagePaths().add(getDataAsString(currentJsonImage, IMAGE_FILE_PATH));
            
            Double[] currentImageDisplacements = new Double[2];
            currentImageDisplacements[0] = getDataAsDouble(currentJsonImage, IMAGE_POX_X);
            currentImageDisplacements[1] = getDataAsDouble(currentJsonImage, IMAGE_POX_Y);
            dataManager.getImageDisplacements().add(currentImageDisplacements);
        }
    }

    @Override
    public void exportData(AppDataComponent data) throws IOException {
        DataManager dataManager = (DataManager)data;
        Workspace workspace = (Workspace)dataManager.getApp().getWorkspaceComponent();
        String rvmDirectory = dataManager.getParentDirectory() + "/" + dataManager.getRegionName() + ".rvm";
        
        boolean allFlagsExist, allLeadersExist, allCapitalsExist;
        
        allFlagsExist = checkIfFlagsExist(dataManager);
        allLeadersExist = checkIfLeadersExist(dataManager);
        allCapitalsExist = checkIfAllCapitalsExists(dataManager);
        
        // EXPOERTING SUBREGION DATA
        JsonArrayBuilder subRegionArrayBuilder = Json.createArrayBuilder();
        ObservableList<SubRegion> subRegions = dataManager.getSubRegionList();
        for(SubRegion subRegion: subRegions){
            JsonObjectBuilder subRegionsObjectBuilder = Json.createObjectBuilder();
            if(!subRegion.getName().equals("") && subRegion.getName() != null)
                subRegionsObjectBuilder.add(SUBREGION_NAME, subRegion.getName());
            
            if(!subRegion.getCapital().equals("") && subRegion.getCapital() != null)
                subRegionsObjectBuilder.add(SUBREGION_CAPITAL, subRegion.getCapital());
            
            if(!subRegion.getLeader().equals("") && subRegion.getLeader() != null)
                subRegionsObjectBuilder.add(SUBREGION_LEADER, subRegion.getLeader());
            
            subRegionsObjectBuilder.add(SUBREGION_RED, (int)(subRegion.getColor().getRed() * 256));
            subRegionsObjectBuilder.add(SUBREGION_GREEN, (int)(subRegion.getColor().getGreen() * 256));
            subRegionsObjectBuilder.add(SUBREGION_BLUE, (int) (subRegion.getColor().getBlue() * 256));
            JsonObject subRegionsObject = subRegionsObjectBuilder.build();
            
            subRegionArrayBuilder.add(subRegionsObject);      
        }
        JsonArray subRegionJsonArray = subRegionArrayBuilder.build();
        
        // PUTTING IT ALL TOGETHER
        JsonObject dataManagerJSON = Json.createObjectBuilder()
                .add(SUBREGION_NAME, dataManager.getRegionName())
                .add(SUBREGIONS_HAVE_CAPITAL, allCapitalsExist)
                .add(SUBREGIONS_HAVE_FLAGS, allFlagsExist)
                .add(SUBREGIONS_HAVE_LEADERS, allLeadersExist)
                .add(SUBREGIONS, subRegionJsonArray).build();
        
        
        Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSON);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(rvmDirectory);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSON);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(rvmDirectory);
	pw.write(prettyPrinted);
	pw.close();
        
        // NOW WE EXPORT THE IMAGE OF THE MAP AS A PNG FILE
        workspace.refreshImages();
        WritableImage image = workspace.getMapPane().snapshot(new SnapshotParameters(), null);
        
        File exportTo = new File(dataManager.getParentDirectory() + "/" + dataManager.getRegionName() + ".png");
        ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", exportTo);
       
    }
    
    public boolean checkIfFlagsExist(DataManager dataManager){
        File currentDir;
        for(SubRegion sub: dataManager.getSubRegionList()){
            currentDir = new File(dataManager.getParentDirectory() + "/" + sub.getName() + " Flag.png");
            if(!currentDir.exists())
                return false;
        }
        
        return true;
    }
    
    public boolean checkIfLeadersExist(DataManager dataManager){
        File currentDir;
        for(SubRegion sub: dataManager.getSubRegionList()){
            if(sub.getLeader().equals("") || sub.getLeader() == null)
                return false;
            
            currentDir = new File(dataManager.getParentDirectory() + "/" + sub.getLeader() + ".png");
            if(!currentDir.exists())
                return false;
            
        }
        
        return true;
    }
    
    public boolean checkIfAllCapitalsExists(DataManager dataManager){
        for(SubRegion sub: dataManager.getSubRegionList()){
            if(sub.getCapital().equals("") || sub.getLeader() == null)
                return false;
        }
        
        return true;
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        
    }

    
    public boolean newData(AppDataComponent data) throws IOException {
        DataManager dataManager = (DataManager)data;
        NewMapDialog newMapDialog = new NewMapDialog(dataManager.getApp());
        String[] array = newMapDialog.showAndGatherInfo(dataManager.getApp().getGUI().getWindow());
        
        if(checkNewMapInfo(array)){
            dataManager.reset();
            dataManager.changeMapPaneBackgroundColor(dataManager.getBackgroundColor());
            
            String regionName = array[0];
            String parentDirectory = array[1] + "/" + regionName;
            String coordinatesDirectory= array[2];
            
            //MAKE NEW DIRECTORY
            File file = new File(parentDirectory);
            file.mkdir();
            
            // LOAD SUB REGIONS INTO THE MAP
            loadSubRegions(dataManager, coordinatesDirectory);
            EditorController controller = new EditorController(dataManager.getApp());
            controller.reassignColors();
            
            // SET NEW REGION NAME
            dataManager.setRegionName(regionName);
            
            // SET NEW PARENT DIRECTORY
            dataManager.setParentDirectory(parentDirectory);
            
            // SET NEW COORDINATE FILE
            dataManager.setCoordinateFilePath(coordinatesDirectory);
            
            // INIT THE INITIAL FILES IN THE DIRECTORY
            exportData(data);
            
            return true;
        }
        else
            return false;
        
    }
    
    /**
     * This will check if the directories returned by the new map dialog exist, if
     * not then a dialog will show telling the user that there was an error when selecting
     * the directories and stop the program from creating the new map.
     * @param array new map info
     * @return if the info checks out
     */
    
    public boolean checkNewMapInfo(String[] array){
        for(String str: array)
            if(str == null)
                return false;
        
        File dir1 = new File(array[1]);
        File dir2 = new File(array[2]);
        
        return dir1.exists() && dir2.exists();
    }
    
    
}
