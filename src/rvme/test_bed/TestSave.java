/**
 * This class will test save data from the regio vinco map editor.
 */
package rvme.test_bed;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javafx.stage.FileChooser;
import properties_manager.PropertiesManager;
import rvme.data.DataManager;
import rvme.file.FileManager;
import static saf.settings.AppPropertyType.SAVE_WORK_TITLE;
import static saf.settings.AppPropertyType.WORK_FILE_EXT;
import static saf.settings.AppPropertyType.WORK_FILE_EXT_DESC;

/**
 *
 * @author Edwin Alvarez
 */
public class TestSave{
    private static DataManager dataManager;
    private static FileManager fileManager;
    
    public static String andorraSavingAbsolutePath = "../HW5SampleData/work/Andorra.json";
    public static String sanMarinoSavingAbsolutePath = "../HW5SampleData/work/San Marino.json";
    public static String slovakiaSavingAbsolutePath = "../HW5SampleData/work/Slovakia.json";
    public static String parentDirectory;
    public static String backgroundColor = "rgb(224, 108, 38)";
    public static String borderColor = "rgb(0, 0, 0)";
    public static double borderWidth = 1;
    public static double mapWidth = 802;
    public static double mapHeight = 536;
    public static double zoomScale = 1;
    public static double[] mapDisplacements = {0, 0}; 
    
    public static double mapDisplacementX = 0;
    public static double mapDisplacementY = 0;
    
    // ANDORRA STUFF
    public static String regionName;
    public static String coordinatePath;
    public static String[] imagesPaths;
    public static ArrayList<Double[]> imageDisplacements;
    public static ArrayList<String[]> subRegionInfo;
    public static ArrayList<Integer[]> subregionColors;
    public static int[] numOfPolygons;
    public static int numOfSubRegions;
    
    public static void main(String[] args) throws IOException {
       
        dataManager = new DataManager(null);
        fileManager = new FileManager();
        
        // THIS SAVES THE ANDORRA DATA INTO A JSON FILE
        saveAndorraData();
        
        // THIS SAVES THE SAN MARINO DATA INTO A JSON FILE
        saveSanMarinoData();
        
        // THIS SAVES THE SLOVAKIA  DATA INTO A JSON FILE
        saveSlovakiaData();
        
    }
    
    public static void saveAndorraData() throws IOException{
        makeAndorraMap();
        dataManager.reset();
        fileManager.loadSubRegions(dataManager, coordinatePath);
        copytoDataManager();
        fileManager.saveData(dataManager, andorraSavingAbsolutePath);
    }
    
    public static void saveSanMarinoData() throws IOException{
        makeSanMarinoMap();
        dataManager.reset();
        fileManager.loadSubRegions(dataManager, coordinatePath);
        copytoDataManager();
        fileManager.saveData(dataManager, sanMarinoSavingAbsolutePath);
    }
    
    public static void saveSlovakiaData() throws IOException{
        makeSlovakiaMap();
        dataManager.reset();
        fileManager.loadSubRegions(dataManager, coordinatePath);
        copytoDataManager();
        fileManager.saveData(dataManager, slovakiaSavingAbsolutePath);
    }
    
    private static void makeAndorraMap(){
        regionName = "Andorra";
        coordinatePath = "../HW5SampleData/raw_map_data/Andorra.json";
        parentDirectory = "../HW5SampleData/export/The World/Europe/Andorra";
        imagesPaths = new String[]{
            "file:../HW5SampleData/work/Andorra COA.png",
            "file:../HW5SampleData/work/Andorra Flag.png"
        };
        imageDisplacements = new ArrayList();
        imageDisplacements.add(new Double[]{8.0,9.0});
        imageDisplacements.add(new Double[]{581.0,390.0});
        numOfSubRegions = 7;
        
        // name, capital, leader
        subRegionInfo = new ArrayList();
        subRegionInfo.add(new String[]{"Ordino", "Ordino (town)", "Ventura Espot"});
        subRegionInfo.add(new String[]{"Canillo", "Canillo (town)", "Enric Casadevall Medrano"});
        subRegionInfo.add(new String[]{"Encamp", "Encamp (town)", "Miquel Alis Font"});
        subRegionInfo.add(new String[]{"Escaldes-Engordany", "Escaldes-Engordany (town)", "Montserrat Capdevila Pallares"});
        subRegionInfo.add(new String[]{"La Massana", "La Massana (town)", "Josep Areny"});
        subRegionInfo.add(new String[]{"Andorra la Vella", "Andorra la Vella (city)", "Maria Rosa Ferrer Obiols"});
        subRegionInfo.add(new String[]{"Sant Julia de Loria", "Sant Julia de Loria (town)", "Josep Pintat Forne"});
        
        subregionColors = new ArrayList();
        subregionColors.add(new Integer[]{200, 200, 200});
        subregionColors.add(new Integer[]{198, 198, 198});
        subregionColors.add(new Integer[]{196, 196, 196});
        subregionColors.add(new Integer[]{194, 194, 194});
        subregionColors.add(new Integer[]{192, 192, 192});
        subregionColors.add(new Integer[]{190, 190, 190});
        subregionColors.add(new Integer[]{188, 188, 188});
        
        numOfPolygons = new int[]{1,1,1,1,1,1,1};
    }
    
    private static void makeSanMarinoMap(){
        regionName = "San Marino";
        coordinatePath = "../HW5SampleData/raw_map_data/SanMarino.json";
        parentDirectory = "../HW5SampleData/export/The World/Europe/San Marino";
        numOfSubRegions = 9;
        imagesPaths = new String[]{
            
        };
        imageDisplacements = new ArrayList();
        
        // name, capital, leader
        subRegionInfo = new ArrayList();
        subRegionInfo.add(new String[]{"Acquaviva", "", "Lucia Tamagnini"});
        subRegionInfo.add(new String[]{"Borgo Maggiore", "", "Sergio Nanni"});
        subRegionInfo.add(new String[]{"Chiesanuova", "", "Franco Santi"});
        subRegionInfo.add(new String[]{"Domagnano", "", "Gabriel Guidi"});
        subRegionInfo.add(new String[]{"Faetano", "", "Pier Mario Bedetti"});
        subRegionInfo.add(new String[]{"Fiorentino", "", "Gerri Fabbri"});
        subRegionInfo.add(new String[]{"Montegiardino", "", "Marta Fabbri"});
        subRegionInfo.add(new String[]{"City of San Marino", "", "Maria Teresa Beccari"});
        subRegionInfo.add(new String[]{"Serravalle", "", "Leandro Maiani"});
        
        subregionColors = new ArrayList();
        subregionColors.add(new Integer[]{225, 225, 225});
        subregionColors.add(new Integer[]{200, 200, 200});
        subregionColors.add(new Integer[]{175, 175, 175});
        subregionColors.add(new Integer[]{150, 150, 150});
        subregionColors.add(new Integer[]{125, 125, 125});
        subregionColors.add(new Integer[]{100, 100, 100});
        subregionColors.add(new Integer[]{75, 75, 75});
        subregionColors.add(new Integer[]{50, 50, 50});
        subregionColors.add(new Integer[]{25, 25, 25});
        
        numOfPolygons = new int[]{1,1,1,1,1,1,1,1,1};
    }
    
    private static void makeSlovakiaMap(){
        regionName = "Slovakia";
        coordinatePath = "../HW5SampleData/raw_map_data/Slovakia.json";
        parentDirectory = "../HW5SampleData/export/The World/Europe/Slovakia";
        imagesPaths = new String[]{
            "file:../HW5SampleData/work/Slovakia COA.png",
            "file:../HW5SampleData/work/Slovakia Flag.png"
        };
        imageDisplacements = new ArrayList();
        imageDisplacements.add(new Double[]{28.0,33.0});
        imageDisplacements.add(new Double[]{543.0,370.0});
        numOfSubRegions = 8;
        
        // name, capital, leader
        subRegionInfo = new ArrayList();
        subRegionInfo.add(new String[]{"Bratislava", "", ""});
        subRegionInfo.add(new String[]{"Trnava", "", ""});
        subRegionInfo.add(new String[]{"Trencin", "", ""});
        subRegionInfo.add(new String[]{"Nitra", "", ""});
        subRegionInfo.add(new String[]{"Zilina", "", ""});
        subRegionInfo.add(new String[]{"Banska Bystrica", "", ""});
        subRegionInfo.add(new String[]{"Presov", "", ""});
        subRegionInfo.add(new String[]{"Kosice", "", ""});
        
        subregionColors = new ArrayList();
        subregionColors.add(new Integer[]{250, 250, 250});
        subregionColors.add(new Integer[]{249, 249, 249});
        subregionColors.add(new Integer[]{248, 248, 249});
        subregionColors.add(new Integer[]{247, 247, 247});
        subregionColors.add(new Integer[]{246, 246, 246});
        subregionColors.add(new Integer[]{245, 245, 245});
        subregionColors.add(new Integer[]{244, 244, 244});
        subregionColors.add(new Integer[]{243, 243, 243});
        
        numOfPolygons = new int[]{2,1,1,1,1,1,1,1};
    }
    
    private static void copytoDataManager(){
        dataManager.setParentDirectory(parentDirectory);
        dataManager.setRegionName(regionName);
        dataManager.setBorderColorString(borderColor);
        dataManager.setBackgroundColorString(backgroundColor);
        dataManager.setBorderWidth(borderWidth);
        dataManager.setMapHeight(mapHeight);
        dataManager.setMapWidth(mapWidth);
        dataManager.setZoomScale(zoomScale);
        
        dataManager.setCoordinateFilePath(coordinatePath);
        dataManager.setMapDisplacements(mapDisplacements);
        
        for(String currentImagePath: imagesPaths){
            dataManager.getImagePaths().add(currentImagePath);
        }
        
        for(Double[] currentImageDisplacements: imageDisplacements)
            dataManager.getImageDisplacements().add(currentImageDisplacements);
        
        for(int i = 0; i < dataManager.getSubRegionList().size(); i++){
            dataManager.getSubRegionList().get(i).setName(subRegionInfo.get(i)[0]);
            dataManager.getSubRegionList().get(i).setCapital(subRegionInfo.get(i)[1]);
            dataManager.getSubRegionList().get(i).setLeader(subRegionInfo.get(i)[2]);
            
            dataManager.getSubRegionList().get(i).setColor(
                    subregionColors.get(i)[0],
                    subregionColors.get(i)[1],
                    subregionColors.get(i)[2]
            );
        }
        
        
            
    }

    public static void setDataManager(DataManager dataManager) {
        TestSave.dataManager = dataManager;
    }

    public static void setFileManager(FileManager fileManager) {
        TestSave.fileManager = fileManager;
    }
    
}
