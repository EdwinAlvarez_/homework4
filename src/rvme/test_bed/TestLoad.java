/**
 * THIS CLASS WILL BE USED TO TEST THE LOADING OF THE LOAD METHOD.
 */
package rvme.test_bed;

import java.io.IOException;
import rvme.data.DataManager;
import rvme.data.SubRegion;
import rvme.file.FileManager;

/**
 *
 * @author Edwin Alvarez
 */
public class TestLoad {
    public static DataManager dataManager;
    private static FileManager fileManager;
    
    private static String andorraJsonFile = "../HW5SampleData/work/Andorra.json";
    private static String sanMarinoJsonFile = "../HW5SampleData/work/San Marino.json";
    private static String slovakiaJsonFile = "../HW5SampleData/work/Slovakia.json";
    
    public static void main(String[] args) throws IOException{
        dataManager = new DataManager(null);
        fileManager = new FileManager();
        
        // THIS LOADS ANDORRA MAP DATA
        loadAndorraData();
        printAllData();
        
        // THIS LOADS SAN MARINO MAP DATA
        loadSanMarinoData();
        printAllData();
        
        // THIS LOADS SLOVAKIA MAP DATA
        loadSlovakiaData();
        printAllData();
    }
    
    public static void loadAndorraData() throws IOException{
        fileManager.loadData(dataManager, andorraJsonFile);
    }
    
    public static void loadSanMarinoData() throws IOException{
        fileManager.loadData(dataManager, sanMarinoJsonFile);
    }
    
    public static void loadSlovakiaData() throws IOException{
        fileManager.loadData(dataManager, slovakiaJsonFile);
    }
    
    private static void printAllData(){
        System.out.println("\n--------------- General Map Data --------------");
        System.out.println("Region Name: " + dataManager.getRegionName());
        System.out.println("Border Width: " + dataManager.getBorderWidth());
        System.out.println("Border Color: " + dataManager.getBorderColorString());
        System.out.println("Background Color: " + dataManager.getBackgroundColorString());
        System.out.println("Map Coordinate File: " + dataManager.getCoordinateFilePath());
        System.out.println("Map Displacements: (" + dataManager.getMapDisplacements()[0] +
                    ", " + dataManager.getMapDisplacements()[1] + ")");
        System.out.println("Number of Sub Regions: " + dataManager.getNumOfSubregions());
        System.out.println("Map Width: " + dataManager.getMapWidth());
        System.out.println("Map Height: " + dataManager.getMapHeight());
        System.out.println("Zoom Scale: " + dataManager.getZoomScale());
        
        System.out.println("\n\n-------------- Sub Regions --------------");
        for(SubRegion currentSubregion: dataManager.getSubRegionList()){
            System.out.println("Sub Region Name: " + currentSubregion.getName());
            System.out.println("Capital: " + currentSubregion.getCapital());
            System.out.println("Leader: " + currentSubregion.getLeader());
            System.out.println("Red: " + (int)(currentSubregion.getColor().getRed() * 256));
            System.out.println("Green: " + (int)(currentSubregion.getColor().getGreen() * 256));
            System.out.println("Blue: " + (int)(currentSubregion.getColor().getBlue() * 256));
            System.out.println("Number of Polygons: " + currentSubregion.getNumOfPolygons() + "\n");
        }
        
        System.out.println("\n\n-------------- All Image Data --------------");
        for(int i = 0; i < dataManager.getImagePaths().size(); i++){
            System.out.println("Image File Path: " + dataManager.getImagePaths().get(i));
            System.out.println("X Position: " + dataManager.getImageDisplacements().get(i)[0]);
            System.out.println("Y Position: " + dataManager.getImageDisplacements().get(i)[1] + "\n");
        }
    }

    public static void setDataManager(DataManager dataManager) {
        TestLoad.dataManager = dataManager;
    }

    public static void setFileManager(FileManager fileManager) {
        TestLoad.fileManager = fileManager;
    }
    
    
}
