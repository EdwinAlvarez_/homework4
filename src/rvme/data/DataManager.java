/**
 * This class handlers all the data in the game. It will have an observable list 
 * of sub regions that will be added or removed to the table view.
 */
package rvme.data;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import rvme.gui.Workspace;
import saf.AppTemplate;
import saf.components.AppDataComponent;

/**
 *
 * @author Edwin Alvarez
 */
public class DataManager implements AppDataComponent{
    
    // THE APP TEMPLATE
    AppTemplate app;
    
    // THE NAME OF THE REGION
    String regionName;
    
    // ARRAY OF ADDED IMAGES (PATH)
    ArrayList<String> imagePaths;
    
    // ARRAY LIST OF IMAGE DISPLACEMENTS
    ArrayList<Double[]> imageDisplacements;
    
    // DISPLACEMENTS OF THE MAP (x, y)
    double[] mapDisplacements;
    
    //  LIST OF SUB REGIONS 
    ObservableList<SubRegion> subRegionList;
    
    // WORKSPACE
    Workspace workspace;
    
    //NUM OF SUB REGIONS
    int numOfSubregions;
    
    // COORDINATE FILE PATH
    String coordinateFilePath;
    
    // BACKGROUND COLOR
    Color backgroundColor;
    String backgroundColorString;
    
    // BORDER COLOR
    Color borderColor;
    String borderColorString;
    
    // BORDER WIDTH
    double borderWidth;
    
    // ZOOM LEVEL
    double zoomScale;
    
    // HEIGHT AND WIDTH OF THE MAP PANE
    static final double DEFAULT_WIDTH = 802.0;
    static final double DEFAULT_HEIGHT = 536.0;
    double mapHeight;
    double mapWidth;
    
    
    // PARENT DIRECTORY
    String parentDirectory;
   
    public DataManager(AppTemplate app){
        this.app = app;
        
        imagePaths = new ArrayList();
        imageDisplacements = new ArrayList();
        mapDisplacements = new double[2];
        
        subRegionList = FXCollections.observableArrayList();
        
        // SET DEFAULT WIDTH AND HEIGHT
        mapWidth = 802;
        mapHeight = 536;
        
        // DEFAULT BORDER COLOR
        borderColor = Color.BLACK;
        
        // DEAFAULT BACKGROUND COLOR
        backgroundColor = Color.color(0/256.0, 172/256.0, 230/256.0);
        
        borderWidth = 1;
        zoomScale = 1;
    }
    
    public void addSubRegion(SubRegion subRegion){
        subRegionList.add(subRegion);
    }
    
    private String getColorValue(Color color){
        int r = (int)(color.getRed() * 256);
        int g = (int)(color.getGreen() * 256);
        int b = (int)(color.getBlue() * 256);
        
        return String.format("rgb(%d, %d, %d)", r, g, b);
    }
    
    public void changeMapPaneBackgroundColor(Color color){
        workspace = (Workspace)app.getWorkspaceComponent();
        backgroundColorString = getColorValue(color);
        workspace.getMapPane().setStyle("-fx-background-color: " + backgroundColorString);
        backgroundColor = color;
    }
    
    public void changeMapPaneBackgroundColorByString(String color){
        workspace = (Workspace)app.getWorkspaceComponent();
        workspace.getMapPane().setStyle("-fx-background-color: " + color);
        backgroundColorString = color;
        backgroundColor = fromStringToColor(backgroundColorString);
        
    }
    
    public Color fromStringToColor(String color){
        int r,g,b;
        String[] stringArray = color.split(" ");
        String redStr = stringArray[0], greenStr = stringArray[1], blueStr = stringArray[2];
        String red,blue, green;
        
        // GETTING RED
        red = "";
        for(int i = 0; i < redStr.length(); i++){
            if(Character.isDigit(redStr.charAt(i)))
                red += redStr.charAt(i);
        }
        r = Integer.parseInt(red);
        
        // GETTING BLUE
        blue = "";
        for(int i = 0; i < blueStr.length(); i++){
            if(Character.isDigit(blueStr.charAt(i)))
                blue += blueStr.charAt(i);
        }
        b = Integer.parseInt(blue);
        
        // GETTING RED
        green = "";
        for(int i = 0; i < greenStr.length(); i++){
            if(Character.isDigit(greenStr.charAt(i)))
                green += greenStr.charAt(i);
        }
        g = Integer.parseInt(green);
        
        return Color.color(r/256.0, g/256.0, b/256.0);
    }
    
    public void changeMapPaneBorderColor(Color color){
        workspace = (Workspace)app.getWorkspaceComponent();
        borderColorString = getColorValue(color);
        
        for(SubRegion currentSubRegion: subRegionList)
            for(Polygon pol: currentSubRegion.getPolygons())
                pol.setStroke(color);
        
        borderColor = color;
    }
    
    public void changeBorderColorByString(String stringColor){
        Color color = fromStringToColor(stringColor);
        
        workspace = (Workspace)app.getWorkspaceComponent();
        borderColorString = stringColor;
        
        for(SubRegion currentSubRegion: subRegionList)
            for(Polygon pol: currentSubRegion.getPolygons())
                pol.setStroke(color);
        
        borderColor = color;
    }
    
    /**
     * This method changes the width of the border width of all polygons inside
     * of the polygon group in the workspace.
     * @param widthValue the new border width of the values
     */
    
    public void changeBorderWidth(double widthValue){
        for(SubRegion currentSubRegion: subRegionList)
            for(Polygon pol: currentSubRegion.getPolygons())
                pol.setStrokeWidth(widthValue);
        
        borderWidth = widthValue;
    }
    
    
    public void moveUp(double px){
        workspace = (Workspace)app.getWorkspaceComponent();
        workspace.getPolygonGroup().setLayoutY(workspace.getPolygonGroup().getLayoutY() - px);
        
        mapDisplacements[1] -= px;
        
    }
    
    public void moveDown(double px){
        workspace = (Workspace)app.getWorkspaceComponent();
        workspace.getPolygonGroup().setLayoutY(workspace.getPolygonGroup().getLayoutY() + px);
        
        mapDisplacements[1] += px;
    }
    
    public void moveLeft(double px){
        workspace = (Workspace)app.getWorkspaceComponent();
        workspace.getPolygonGroup().setLayoutX(workspace.getPolygonGroup().getLayoutX() - px);
        
        mapDisplacements[0] -= px;
    }
    
    public void moveRight(double px){
        workspace = (Workspace)app.getWorkspaceComponent();
        workspace.getPolygonGroup().setLayoutX(workspace.getPolygonGroup().getLayoutX() + px);
        
        mapDisplacements[0] += px;
    }
    
    public void setSubRegionToSelected(SubRegion subRegion){
        workspace = (Workspace)app.getWorkspaceComponent();
        
        for(Polygon pol: subRegion.getPolygons()){
            pol.setFill(Color.YELLOW);
            pol.setStroke(Color.YELLOW);
            workspace.getPolygonGroup().getChildren().remove(pol);
            workspace.getPolygonGroup().getChildren().add(workspace.getPolygonGroup().getChildren().size() - 1, pol);
        }
    }
    
    
    public double getMapArea(){
        double xMin = DEFAULT_WIDTH, yMin = DEFAULT_HEIGHT, xMax = 0, yMax = 0;
        
        for(SubRegion sub: subRegionList)
            for(Polygon pol: sub.getPolygons()){
                Object[] points = pol.getPoints().toArray();
                for(int i = 0; i < points.length; i += 2){
                    double currentX = ((Double)points[i]).doubleValue();
                    double currentY = ((Double)points[i + 1]).doubleValue();
                    
                    
                    if(currentX > xMax)
                        xMax = currentX;
                    else if(currentX < xMin)
                        xMin = currentX;
                    
                    if(currentY > yMax)
                        yMax = currentY;
                    else if(currentY < yMin)
                        yMin = currentY;
                    
                }
            }
        double x = xMax - xMin;
        double y = yMax - yMin;
        
        return x * y;
                    
    }

    public AppTemplate getApp() {
        return app;
    }

    public String getParentDirectory() {
        return parentDirectory;
    }

    public String getRegionName() {
        return regionName;
    }

    public ArrayList<String> getImagePaths() {
        return imagePaths;
    }

    public ObservableList<SubRegion> getSubRegionList() {
        return subRegionList;
    }
    
    public ArrayList<Double[]> getImageDisplacements() {
        return imageDisplacements;
    }

    public double[] getMapDisplacements() {
        return mapDisplacements;
    }

    public int getNumOfSubregions() {
        return numOfSubregions;
    }

    public String getCoordinateFilePath() {
        return coordinateFilePath;
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public Color getBorderColor() {
        return borderColor;
    }

    public double getBorderWidth() {
        return borderWidth;
    }

    public double getZoomScale() {
        return zoomScale;
    }

    public double getMapHeight() {
        return mapHeight;
    }

    public double getMapWidth() {
        return mapWidth;
    }

    public String getBackgroundColorString() {
        return backgroundColorString;
    }

    public String getBorderColorString() {
        return borderColorString;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public void setMapDisplacements(double[] mapDisplacements) {
        this.mapDisplacements = mapDisplacements;
    }

    public void setNumOfSubregions(int numOfSubregions) {
        this.numOfSubregions = numOfSubregions;
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public void setBorderColor(Color borderColor) {
        this.borderColor = borderColor;
    }

    public void setBorderWidth(double borderWidth) {
        this.borderWidth = borderWidth;
    }

    public void setZoomScale(double zoomScale) {
        this.zoomScale = zoomScale;
    }

    public void setMapHeight(double height) {
        this.mapHeight = height;
    }

    public void setMapWidth(double width) {
        this.mapWidth = width;
    }

    public void setCoordinateFilePath(String coordinateFilePath) {
        this.coordinateFilePath = coordinateFilePath;
    }

    public void setBackgroundColorString(String backgroundColorString) {
        this.backgroundColorString = backgroundColorString;
    }

    public void setBorderColorString(String borderColorString) {
        this.borderColorString = borderColorString;
    }

    public void setParentDirectory(String parentDirectory) {
        this.parentDirectory = parentDirectory;
    }
    
    
    @Override
    public void reset() {
        imagePaths.clear();
        imageDisplacements.clear();
        mapDisplacements = new double[2];
        subRegionList.clear();
        mapWidth = 802;
        mapHeight = 536;
        borderColor = Color.BLACK;
        backgroundColor = Color.color(0/256.0, 172/256.0, 230/256.0);
        borderColorString = getColorValue(borderColor);
        backgroundColorString = getColorValue(backgroundColor);
        
        coordinateFilePath = null;
        borderWidth = 1;
        zoomScale = 1;
        regionName = null;
    }
    
}
