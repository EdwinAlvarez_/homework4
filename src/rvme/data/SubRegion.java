/*
 * This class will represent a sub region ans it's data, this class extends
 * the polygons class so it will also create the polygon and return it's current
 * data.
 */
package rvme.data;

import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.image.Image;
import rvme.gui.Workspace;
import saf.AppTemplate;

/**
 *
 * @author Edwin Alvarez
 * @version 1.0
 */
public class SubRegion{
    
    // DEFAULT STRING
    final static String DEFAULT_STRING = "";
    
    //POLYGON DATA
    int numOfPolygons;
    ArrayList<Polygon> polygons;
    
    // TABLEVIEW ITEMS
    final StringProperty name;
    final StringProperty capital;
    final StringProperty leader;
    
    //IMAGES
    Image flagImage;
    Image leaderImage;
    
    // Color
    Color color;
    
    public SubRegion(){
        // POLYGON LIST
        polygons = new ArrayList();      
        
        // SET TO DEFAULT
        this.name = new SimpleStringProperty(DEFAULT_STRING);
        this.capital = new SimpleStringProperty(DEFAULT_STRING);
        this.leader = new SimpleStringProperty(DEFAULT_STRING);
    }
    
    public SubRegion(String name, String capital, String leader, int[] rgb){
        this();
        
        // TABLE INFO
        this.name.set(name);
        this.capital.set(capital);
        this.leader.set(leader);

        // INITIATES THE  COLOR
        setColor(rgb[0], rgb[1], rgb[2]);
    }
    
    public void addPolygon(ArrayList<Double> list, double paneHeight, double paneWidth){
        
        list = setWorkspaceCordinates(list, paneHeight, paneWidth);
        
        Polygon pol = new Polygon();
        pol.getPoints().addAll(list);
        
        pol.setFill(Color.DARKSEAGREEN);
        pol.setStroke(Color.BLACK);
        polygons.add(pol);
    }
    
    private ArrayList<Double> setWorkspaceCordinates(
            ArrayList<Double> list, double height, double width){
        double globeWidth = 360.0;
        double globeHeight = 180.0;
        
        // TRANSLATE MAP CORDINATE INTO WORKSPACE COORINATES
        for(int i = 0; i < list.size(); i += 2){
            
            list.set(i, ((list.get(i) + 180)/ globeWidth) * width);   // x
            double y = ((list.get(i + 1) + 90)/ globeHeight) * height;
            y = Math.abs(y - height);
            list.set(i + 1, y);  // y
        }
        
        return list;
    }
    
    
    public void setColor(int r, int g, int b){
        color = Color.color(r/254.0, g/254.0, b/254.0);
        for(Polygon pol: polygons)
            pol.setFill(color);
    }
    
    public void changeColor(Color newColor){
        for(Polygon pol: polygons)
            pol.setFill(newColor);
        color = newColor;
    }
    
    public void changePolygonAndBorderColor(Color PolygonColor, Color borderColor){
        for(Polygon pol: polygons){
            pol.setFill(color);
            pol.setStroke(borderColor);
        }
    }
    

    public Color getColor() {
        return color;
    }

    public String getName() {
        return name.get();
    }

    public String getCapital() {
        return capital.get();
    }

    public String getLeader() {
        return leader.get();
    }

    public Image getFlagImage() {
        return flagImage;
    }

    public Image getLeaderImage() {
        return leaderImage;
    }

    public ArrayList<Polygon> getPolygons() {
        return polygons;
    }
    

    public void setCapital(String capital) {
        this.capital.set(capital);
    }

    public void setLeader(String leader) {
        this.leader.set(leader);
    }

    public void setFlagImage(String flagImagePath) {
        this.flagImage = new Image(flagImagePath);
    }

    public void setLeaderImage(String leaderImagePath) {
        this.leaderImage = new Image(leaderImagePath);
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public int getNumOfPolygons() {
        return numOfPolygons;
    }

    public void setNumOfPolygons(int numOfPolygons) {
        this.numOfPolygons = numOfPolygons;
    }
    
    
    
    
}
