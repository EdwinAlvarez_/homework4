/**
 * This class will contain start up constants that will be used for styling.
 */
package rvme;

/**
 *
 * @author Edwin Alvarez
 * @version 1.0
 */
public class StartConstants {
    
    public static final String COLOR_PICKER_STYLE = "edit_toolbar_color_picker";
    
    /**
     * 
     */
    public static final String MAP_PANE_STYLE = "map_pane";
    
    /**
     * 
     */
    public static final String MAP_PARENT_PANE_STYLE = "parent_map_pane";
    
    /**
     * 
     */
    public static final String TOOLBAR_SPACE_STYLE = "toolbar_space";
    
    /**
     * 
     */
    public static final String VALUE_NAME = "name";
    
    /**
     * 
     */
    public static final String VALUE_CAPITAL = "capital";
    
    /**
     * 
     */
    public static final String VALUE_LEADER = "leader";
    
    /**
     *
     */
     public static final String CLASS_BORDERED_PANE = "bordered_pane";
     
     /**
      * 
      */
     
     public static final String BACKGROUND_STYLING = "app_background";
     
     /**
      * 
      */
     
     public static final String DARK_BAGROUND_STYLING_SPACE = "dark_app_background_space";
     
     /**
      * 
      */
     
     public static final String BACKGROUND_STYLING_BUTTON = "app_background_button";
     
     /**
      * 
      */
     
     public static final String WHITE_LABEL_STYLING = "white_label";
     
}
