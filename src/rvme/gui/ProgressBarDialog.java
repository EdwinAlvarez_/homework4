/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme.gui;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import rvme.PropertyType;
import saf.AppTemplate;

/**
 *
 * @author Edwin Alvarez
 */
public class ProgressBarDialog {
    // APP TEMPLATE
    AppTemplate app;
    
    // WINDOW
    Stage window;
    
    // SCENE
    Scene scene;
    
    // PROPERTIES MANAGER
    PropertiesManager pro;
    
    //DIALOG CONTENT
    Label progressLabel;
    
    // PROGRESS BAR
    ProgressBar progressBar;
    
    // PANE
    VBox pane;
    
    public ProgressBarDialog(AppTemplate app, ProgressBar progressBar){
        
        this.app = app;
        this.progressBar = progressBar;
        
        initWindow();
    }

    private void initWindow() {
        
       window = new Stage();
       window.initModality(Modality.WINDOW_MODAL);
       window.setResizable(false);
       
       //INIT PROPERTIES MANANGER
       pro = PropertiesManager.getPropertiesManager();
       
       // INIT VBOX
       pane = new VBox(10);
       
       // INIT LABEL
       progressLabel = new Label(pro.getProperty(PropertyType.PROGRESS_BAR_LABEL));
       
       pane.getChildren().addAll(progressLabel, progressBar);
       pane.setAlignment(Pos.CENTER);
       initListeners();
       
       
       scene = new Scene(pane,350,200);
       window.setScene(scene);
       window.setTitle(pro.getProperty(PropertyType.PROGRESS_BAR_TITLE));
    }
    
    public void showProgress(){
        window.initOwner(app.getGUI().getWindow());
        window.showAndWait();
    }

    private void initListeners() {
        progressBar.progressProperty().addListener((e, newVal, oldVal) -> {
            if(newVal.doubleValue() >= 1){
                window.close();
            }
        });
    }
}
