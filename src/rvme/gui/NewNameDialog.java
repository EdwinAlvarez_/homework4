/**
 * This class prompts the user for a new name for the region.
 */
package rvme.gui;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import rvme.PropertyType;
import rvme.StartConstants;
import rvme.data.DataManager;
import saf.AppTemplate;

/**
 *
 * @author Edwin Alvarez
 * @version 1.0
 */

public class NewNameDialog {
    // APP TEMPLATE
    AppTemplate app;
    
    // WINDOW
    Stage window;
    
    // SCENE
    Scene scene;
    
    // PROPERTIES MANAGER
    PropertiesManager pro;
    
    //DIALOG CONTENT
    Label newNameLabel;
    TextField newNameTextField;
    Button continueButton;
    GridPane pane;
    
    // THE NEW NAME
    String newName;
    
    public NewNameDialog(AppTemplate initApp){
        app = initApp;
        
        initWindow();
        initHandlers();
        initStyleSheet();
        initStyle();
    }

    private void initWindow() {
       window = new Stage();
       window.initModality(Modality.WINDOW_MODAL);
       window.setResizable(false);
       
       // DATA MANAGER
       DataManager dataManager = (DataManager)app.getDataComponent();
       
       //INIT PROPERTIES MANANGER
       pro = PropertiesManager.getPropertiesManager();
       
       // INIT CONTENTS
       newNameLabel = new Label(pro.getProperty(PropertyType.NEW_REGION_NAME_MESSAGE));
       continueButton = new Button(pro.getProperty(PropertyType.BUTTON_CONTINUE));
       newNameTextField = new TextField();
       pane = new GridPane();
       pane.setPadding(new Insets(10,10,10,10));
       newNameTextField.setText(dataManager.getRegionName());

       
       //INIT PANE
       pane.setVgap(8);
       pane.setHgap(10);
       pane.add(newNameLabel, 0, 0);
       pane.add(newNameTextField, 1, 0);
       pane.add(continueButton, 0, 1);
       GridPane.setHalignment(continueButton, HPos.LEFT);
       pane.setAlignment(Pos.CENTER);
       
       scene = new Scene(pane,350,200);
       window.setScene(scene);
       window.setTitle(pro.getProperty(PropertyType.REGION_NAME_DIALOG_TITLE));
       
       
       
    }

    private void initStyleSheet() {
        // SELECT THE STYLESHEET
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String stylesheet = props.getProperty(PropertyType.WORKSPACE_PATH_CSS);
	scene.getStylesheets().add(stylesheet);
    }

    private void initStyle() {
        pane.getStyleClass().add(StartConstants.CLASS_BORDERED_PANE);
        newNameLabel.getStyleClass().add(StartConstants.WHITE_LABEL_STYLING);
    }
    
    public String getNameAndShow(){
        window.initOwner(app.getGUI().getWindow());
        window.showAndWait();
        
        return newName;
    }

    private void initHandlers() {
        continueButton.setOnAction(e -> {
            getNewName();
            window.close();
        });
    }

    private void getNewName() {
        newName = newNameTextField.getText();

    }
}
