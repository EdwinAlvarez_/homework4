/**
 * This class will prompt the user to enter new information about the map when the
 * user wants to start a new project. The method in this class should return an array
 * of string, one for the coordinates and one for the parent directory and one for
 * the name of the map.
 */
package rvme.gui;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import rvme.PropertyType;
import rvme.StartConstants;
import rvme.data.DataManager;
import saf.AppTemplate;

/**
 *
 * @author Edwin Alvarez
 * @version 1.0
 */
public class NewMapDialog {
    // FIGURE OUT WHEATHER WE WILL CREATE THE DIRECTORY HERE OR IN THE CALLING MEHTOD
    // THAT WILL DETERMINE WHAT THIS CLASS WILL RETURN
    
    // APP TEMPLATE
    AppTemplate app;
    
    // DATA MANAGER
    DataManager dataManager;
    
    // DIALOG CONTENT
    Label parentDirectoryLabel;
    Label coordinatedataDirectoryLabel;
    Label nameLabel;
    FileChooser fileChooser;
    DirectoryChooser directoryChooser;

    Button okButton;
    GridPane pane;
    TextField nameTextField;
    TextField coordinateDataTextField;
    TextField parentTextField;
    Button parentDirButton;
    Button coordinateDataDirButton;
    HBox parentDirPane;
    HBox coordinateDirPane;
    
    //FILE PATHS
    String newDirectoryFilePath;
    String coordinateFilePath;
    String[] filePathArray;
    
    
    // WINDOW
    Stage window;
    
    // SCENE
    Scene scene;
    
    // PROPERTIES MANAGER
    PropertiesManager pro;
    
    public NewMapDialog(AppTemplate initApp){
        app = initApp;
        filePathArray = new String[3];
        
        initWindow();
        initHandlers();
        initStyleSheet();
        initStyle();
    }

    private void initWindow() {
       window = new Stage();
       window.initModality(Modality.WINDOW_MODAL);
       window.setResizable(false);
       
       //INIT PROPERTIES MANANGER
       pro = PropertiesManager.getPropertiesManager();
       
       pane = new GridPane();
       pane.setVgap(8);
       pane.setHgap(10);
       pane.setPadding(new Insets(10,10,10,10));
       
       // INIT LABELS
       parentDirectoryLabel = new Label(pro.getProperty(PropertyType.NEW_REGION_PARENT_DIR));
       coordinatedataDirectoryLabel = new Label(pro.getProperty(PropertyType.NEW_REGION_COORDINATE_DIR));
       nameLabel = new Label(pro.getProperty(PropertyType.NEW_REGION_NAME));
       
       // INIT TEXTFIELDS
       nameTextField = new TextField();
       parentTextField = new TextField();
       coordinateDataTextField = new TextField();
       parentTextField.setPromptText(pro.getProperty(PropertyType.BUTTON_BROWSE_COMPUTER));
       coordinateDataTextField.setPromptText(pro.getProperty(PropertyType.BUTTON_BROWSE_COMPUTER));
       
       //INIT FILECHOOSERS
       fileChooser = new FileChooser();
       directoryChooser = new DirectoryChooser();
       
       // INIT PANES
       parentDirPane = new HBox(5);
       coordinateDirPane = new HBox(5);
       
       // INIT BUTTON
       parentDirPane.getChildren().add(parentTextField);
       coordinateDirPane.getChildren().add(coordinateDataTextField);
       okButton = new Button(pro.getProperty(PropertyType.BUTTON_CONTINUE));
       parentDirButton = app.getGUI().initChildButton(parentDirPane, PropertyType.FOLDER_ICON.toString(), PropertyType.LOAD_TOOLTIP_W.toString(), false);
       coordinateDataDirButton = app.getGUI().initChildButton(coordinateDirPane, PropertyType.FILE_ICON.toString(), PropertyType.LOAD_TOOLTIP_W.toString(), false);
       
       
       //ADDING CHILDREN
       pane.add(nameLabel, 0, 0);
       pane.add(nameTextField, 1, 0);
       pane.add(parentDirectoryLabel, 0, 1);
       pane.add(parentDirPane, 1, 1);
       pane.add(coordinatedataDirectoryLabel, 0, 2);
       pane.add(coordinateDirPane, 1, 2);
       pane.add(okButton, 1, 3);
       
       GridPane.setHalignment(okButton, HPos.RIGHT);
       GridPane.setHalignment(nameTextField, HPos.RIGHT);
       GridPane.setHalignment(parentDirButton, HPos.RIGHT);
       GridPane.setHalignment(coordinateDataDirButton, HPos.RIGHT);
       
       pane.setAlignment(Pos.CENTER);
       
       
       scene = new Scene(pane,400,200);
       window.setScene(scene);
       window.setTitle(pro.getProperty(PropertyType.NEW_REGION_TITLE));
    }

    private void initStyleSheet() {
        // SELECT THE STYLESHEET
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String stylesheet = props.getProperty(PropertyType.WORKSPACE_PATH_CSS);
	scene.getStylesheets().add(stylesheet);
    }

    private void initStyle() {
        pane.getStyleClass().add(StartConstants.BACKGROUND_STYLING);
        parentDirButton.getStyleClass().add(StartConstants.BACKGROUND_STYLING_BUTTON);
        coordinateDataDirButton.getStyleClass().add(StartConstants.BACKGROUND_STYLING_BUTTON);
        parentDirectoryLabel.getStyleClass().add(StartConstants.WHITE_LABEL_STYLING);
        coordinatedataDirectoryLabel.getStyleClass().add(StartConstants.WHITE_LABEL_STYLING);
        nameLabel.getStyleClass().add(StartConstants.WHITE_LABEL_STYLING);
    }
    
    public String[] showAndGatherInfo(Stage parentWindow){
        window.initOwner(parentWindow);
        window.showAndWait();
        
        // NAME, PARENT DIR, COORDINATE DIR 
        return filePathArray;
    }

    private void initHandlers() {
        okButton.setOnAction(e ->{
            constructStringArray();
        });
        
        parentDirButton.setOnAction(e ->{
            setTextParentDir();
        });
        
        coordinateDataDirButton.setOnAction(e -> {
            setTextCoordinateDir();
        });
    }
    
    private void setTextCoordinateDir(){
        File coordinates = fileChooser.showOpenDialog(window);
        String currentDirectory = Paths.get(".").toAbsolutePath().normalize().toString();
        
        // GETTING PATHS
        Path coordinatePath = Paths.get(coordinates.getAbsolutePath());
        Path currentPath = Paths.get(currentDirectory);
        
        // EXTRACT THE RELATIVE PATH
        filePathArray[2] = currentPath.relativize(coordinatePath).toString();
        coordinateDataTextField.setText(coordinates.getAbsolutePath());
    }
    
    private void setTextParentDir(){
        File parentDir = directoryChooser.showDialog(window);
        String currentDirectory = Paths.get(".").toAbsolutePath().normalize().toString();
        
        // GETTING PATHS
        Path parentPath = Paths.get(parentDir.getAbsolutePath());
        Path currentPath = Paths.get(currentDirectory);
        
        // EXTRACT THE RELATIVE PATH
        filePathArray[1] = currentPath.relativize(parentPath).toString();
        parentTextField.setText(parentDir.getAbsolutePath());
    }
    
    private void constructStringArray(){
        filePathArray[0] = nameTextField.getText();
        window.close();
    }
    
    
}
