/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme.gui;

import audio_manager.AudioManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import properties_manager.PropertiesManager;
import rvme.PropertyType;
import rvme.data.DataManager;
import rvme.data.SubRegion;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import saf.ui.AppGUI;
import rvme.StartConstants;
import rvme.controller.EditorController;
import rvme.file.FileManager;
import static saf.settings.AppPropertyType.LOAD_ERROR_MESSAGE;
import static saf.settings.AppPropertyType.LOAD_ERROR_TITLE;
import saf.ui.AppMessageDialogSingleton;


/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Edwin Alvarez
 * @version 1.0
 */

public class Workspace extends AppWorkspaceComponent{
    
    // APP TEMPLATE AND COMPONENTS
    DataManager dataManager;
    AppTemplate app;
    
    //EDIT TOOL BAR CONTROLS
    HBox editToolBar;
    Button changeNameButton,
    addImageButton,
    reassignColorButton,
    changeColorButton,
    audioButton,
    stopAudioButton,
    removeButton,
    changeDimensionsButton;
    
    AudioManager audioManager;
    
    ToolBar toolBar;
    
    ColorPicker backgroundColorPicker,
    borderColorPicker;
    
    Slider borderWidthSlider,
    zoomSlider;
    
    Label zoomingLabel,
    borderThicknessLabel,
    boderColorLabel,
    backgroundColorLabel;
    
    // FILE TOOLBAR PANE
    HBox appPane;
    
    // CLIP IN ORDER TO AVOID OVERFLOW FOR THE MAP
    Rectangle clip;
    
    //TABLE VIEW COLUMNS, ITEMS AND PANES
    TableView<SubRegion> tableView;
    TableColumn regionNameColumn,
    leaderNameColumn,
    capitalNameColumn;
    
    // MAP PANE
    ScrollPane scrollPane;
    StackPane parentMapPane;
    Pane mapPane;
    
    //SPLIT PANE
    SplitPane pane;
    
    //ARRAY OF IMAGS
    ArrayList<ImageView> images;
    
    // IMAGE CURRENTLY SELECTED
    ImageView currentImage;
    
    // SUB REGION DIALOG BOX FOR EDIT AND VIEWING
    SubRegionDialogBox subRegionDialog;
    
    //THINGS TO SHOW WHILE FILE ARE BEING LOADED INTO THE WORKSPACE
    ProgressBar progressBar;
    DoubleProperty progress;
    
    // GROUP
    Group polygonGroup;
    
    //IS MUSIC PLAYING?
    boolean isMusicPlaying;
    
    //INDEX FOR UPDATING THE IMAGE DISPLACEMENTS
    int currentIndex;
    
    //POLYGON TO SUBREGION 
    HashMap<Polygon, SubRegion> polToSubHashMap;
    
    //GUI
    AppGUI gui;
    
    public Workspace(AppTemplate app){
        this.app = app;
        
        dataManager = (DataManager)app.getDataComponent();
        gui = app.getGUI();
        
        layoutGui();
        setUpHandlers();
        setListeners();
        initStyle();
    }
    
    private void layoutGui(){
        PropertiesManager prop = PropertiesManager.getPropertiesManager();
        initEditToolBar();
        
        //CREATING THE TABLEVIEW
        tableView = new TableView();
        
        //MUSIC IS NOT PLAYING NOW
        isMusicPlaying = false;
        
        //INIT THE HASHMAP
        polToSubHashMap = new HashMap();
        
        regionNameColumn = new TableColumn(prop.getProperty(PropertyType.COLUMN_SUBREGION));
        leaderNameColumn = new TableColumn(prop.getProperty(PropertyType.COLUMN_LEADER));
        capitalNameColumn = new TableColumn(prop.getProperty(PropertyType.COLUMN_CAPITAL));
        
        regionNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("name"));
        leaderNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("leader"));
        capitalNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("capital"));
        
        tableView.getColumns().addAll(regionNameColumn, leaderNameColumn, capitalNameColumn);
        
        
        // MAP PANE
        mapPane = new Pane();
        
        //INIT IMAGES ARRAY
        images = new ArrayList();
        
        // INIT AUDIO MANAGER
        audioManager = new AudioManager();
        
        // ADD A CLIP TO AVOID OVERFLOW
        clip = new Rectangle();
        clip.widthProperty().bind(mapPane.widthProperty());
        clip.heightProperty().bind(mapPane.heightProperty());
        mapPane.setClip(clip);
        
        // SETTING THE WORKING 
        scrollPane = new ScrollPane();
        parentMapPane = new StackPane();
        scrollPane.setContent(parentMapPane);
        scrollPane.setFocusTraversable(false);
        pane = new SplitPane();
        StackPane tableViewPane = new StackPane();
        parentMapPane.getChildren().add(mapPane);
        tableViewPane.getChildren().add(tableView);
        
        pane.getItems().addAll(scrollPane, tableViewPane);
        pane.setDividerPositions(0.5f);
        
        //BINDING THE HEIGHT OF THE PARENT MAP PANE
        parentMapPane.prefHeightProperty().bind(pane.heightProperty());
        
        
        //BINDING THE TABLE COLUMNS TO THE WIDTH OF THE STACK PANE
        regionNameColumn.prefWidthProperty().bind(tableViewPane.widthProperty().multiply(0.33));
        leaderNameColumn.prefWidthProperty().bind(tableViewPane.widthProperty().multiply(0.33));
        capitalNameColumn.prefWidthProperty().bind(tableViewPane.widthProperty().multiply(0.33));
        
        // INIT GROUP
        polygonGroup = new Group();
        polygonGroup.scaleXProperty().bind(zoomSlider.valueProperty());
        polygonGroup.scaleYProperty().bind(zoomSlider.valueProperty());
        
        
        setListeners();
        
    }
    
    /**
     * THIS METHOD WILL JUST INITIALIZE SAMPLE DATA TO SHOW HOW THE APP WOULD LOOK
     * TO BE DELETED LATER***
     */
    

    public TableView<SubRegion> getTable() {
        return tableView;
    }

    
    private void setUpHandlers() {
        PropertiesManager prop = PropertiesManager.getPropertiesManager();
        EditorController controller = new EditorController(app);
        
        tableView.setRowFactory(e -> {
            TableRow<SubRegion> tableRow = new TableRow<>();
            tableRow.setOnMouseClicked(a -> {
                if (a.getClickCount() == 2 && !tableRow.isEmpty()) {
                    controller.viewAndEditSubregion();
                    tableView.getColumns().get(0).setVisible(false);
                    tableView.getColumns().get(0).setVisible(true);
                    tableView.getColumns().get(1).setVisible(false);
                    tableView.getColumns().get(1).setVisible(true);
                    tableView.getColumns().get(2).setVisible(false);
                    tableView.getColumns().get(2).setVisible(true);
                }
            });
            return tableRow;
        });
        
        audioButton.setOnAction(e -> {
            if(isMusicPlaying)
                controller.stopAudio();
            else
                controller.playAudio();
        });
        
        addImageButton.setOnAction(e ->{
            controller.addImage();
        });
        
        changeNameButton.setOnAction(e ->{
            controller.changeRegionName();
        });
        
        changeDimensionsButton.setOnAction(e ->{
            controller.changeMapDiemsions();
        });
        
        reassignColorButton.setOnAction(e -> {
            controller.reassignColors();
            app.getGUI().updateToolbarControls(false);
        });
        
        changeColorButton.setOnAction(e -> {
            controller.changeMapColors();
        });
        
        removeButton.setOnAction(e -> {
            controller.removeImage();
            currentImage = null;
        });
        
        scrollPane.setOnKeyPressed(e -> {
            e.consume();
            switch (e.getCode()) {
            case UP:
                dataManager.moveUp(15);
                break;
            case DOWN:
                dataManager.moveDown(15);
                break;
            case LEFT:
                dataManager.moveLeft(15);
                break;
            case RIGHT:
                dataManager.moveRight(15);
                break;
            default:
                break;
            }
            
            app.getGUI().updateToolbarControls(false);
        });
        
        tableView.setOnMouseClicked(e ->{
            if(!tableView.getSelectionModel().isEmpty()){
                reloadSubregionColors();
                dataManager.setSubRegionToSelected(tableView.getSelectionModel().getSelectedItem());
                refreshImages();
            }
        });
        
        tableView.setOnKeyPressed(e ->{
            if(!tableView.getSelectionModel().isEmpty()){
                reloadSubregionColors();
                dataManager.setSubRegionToSelected(tableView.getSelectionModel().getSelectedItem());
            }
        });
        
        tableView.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue)->{
            if(!newValue)
                reloadSubregionColors();
        });
        
        zoomSlider.setOnMouseReleased(e -> {
            bindAllBorders();
        });

    }
    

    public Pane getMapPane() {
        return mapPane;
    }

    private void setListeners() {
        
        //THIS KEEPS THE WIDTH OF THE PARENT STACK PANE RELATIBE TOP THE FIRST HALF OF THE WINDOW'S WIDTH
        pane.getDividers().get(0).positionProperty().addListener(e ->{
            parentMapPane.setPrefWidth((pane.getWidth() * pane.getDividers().get(0).getPosition()) - 25);
        });
        
        audioButton.widthProperty().addListener((e, oldVal, newVal) ->{
            audioButton.setPrefWidth(oldVal.doubleValue());
        });

        zoomSlider.valueProperty().addListener(e -> {
            dataManager.setZoomScale(zoomSlider.getValue());
            
            app.getGUI().updateToolbarControls(false);
        });
        
        borderWidthSlider.valueProperty().addListener(e -> {
            dataManager.setBorderWidth(borderWidthSlider.getValue());
            
            app.getGUI().updateToolbarControls(false);
        });
        
    }
    
    public void initEditToolBar(){
        PropertiesManager prop = PropertiesManager.getPropertiesManager();
        
        
        editToolBar = new HBox();
        HBox nonButtonSection = new HBox(5);
        HBox buttonSection = new HBox();
        
        backgroundColorPicker = new ColorPicker();
        borderColorPicker = new ColorPicker();
        backgroundColorPicker.setDisable(true);
        borderColorPicker.setDisable(true);
    
        borderWidthSlider = new Slider();
        zoomSlider = new Slider();
        borderWidthSlider.setPrefWidth(100);
        borderWidthSlider.setMax(10);
        borderWidthSlider.setMin(1);
        zoomSlider.setPrefWidth(100);
        zoomSlider.setDisable(true);
        zoomSlider.setMin(1);
        borderWidthSlider.setDisable(true);
    
        // SETTING UP LABELS
        zoomingLabel = new Label(prop.getProperty(PropertyType.ZOOM_LABEL));
        borderThicknessLabel = new Label(prop.getProperty(PropertyType.BORDER_THICKNESS_LABEL));
        boderColorLabel = new Label(prop.getProperty(PropertyType.BORDER_COLOR_LABEL));
        backgroundColorLabel = new Label(prop.getProperty(PropertyType.BANCKGROUND_COLOR_LABEL));
        
        GridPane gPane = new GridPane();
        gPane.setHgap(10);
        gPane.setVgap(8);
        gPane.add(zoomingLabel, 0, 0);
        gPane.add(zoomSlider, 1, 0);
        gPane.add(borderThicknessLabel, 0, 1);
        gPane.add(borderWidthSlider, 1, 1);
        GridPane.setHalignment(zoomingLabel, HPos.RIGHT);
        
        nonButtonSection.getChildren().addAll(gPane);
        nonButtonSection.getChildren().addAll(boderColorLabel, borderColorPicker, backgroundColorLabel, backgroundColorPicker);
        nonButtonSection.setAlignment(Pos.CENTER);
        
        // SETTING UP BUTTONS
        changeColorButton = gui.initChildButton(nonButtonSection, PropertyType.CHANGE_COLORS_ICON.toString(), PropertyType.CHANGE_COLORS_TOOLTIP.toString(), true);
        changeNameButton = gui.initChildButton(buttonSection, PropertyType.CHANGE_NAME_ICON.toString(), PropertyType.CHANGE_NAME_TOOLTIP.toString(), true);
        reassignColorButton = gui.initChildButton(buttonSection, PropertyType.REASSIGN_ICON.toString(), PropertyType.REASSIGN_TOOLTIP.toString(), true);
        audioButton = gui.initChildButton(buttonSection, PropertyType.PLAY_AUDIO_ICON.toString(), PropertyType.PLAY_SONG_TOOLTIP.toString(), true);
        removeButton = gui.initChildButton(buttonSection, PropertyType.REMOVE_ICON.toString(), PropertyType.REMOVE_IMAGE_TOOLTIP.toString(), true);
        addImageButton = gui.initChildButton(buttonSection, PropertyType.NEW_IMAGE_ICON.toString(), PropertyType.NEW_IMAGE_TOOLTIP.toString(), true);
        changeDimensionsButton = gui.initChildButton(buttonSection, PropertyType.DIMENSIONS_ICON.toString(), PropertyType.DIMENSIONS_TOOLTIP.toString(), true);
        addImageButton.setPrefHeight(38);
        changeNameButton.setPrefHeight(38);
        reassignColorButton.setPrefHeight(38);
        audioButton.setPrefHeight(38);
        changeColorButton.setPrefHeight(38);
        removeButton.setPrefHeight(38);
        changeDimensionsButton.setPrefHeight(38);
        changeColorButton.setPrefHeight(38);
        buttonSection.setAlignment(Pos.CENTER);
        
        editToolBar.getChildren().addAll(nonButtonSection, buttonSection);
        appPane = (HBox)gui.getAppPane().getTop();
        appPane.setAlignment(Pos.CENTER);

        Pane space = new Pane();
        HBox.setHgrow(space, Priority.SOMETIMES);
        toolBar = new ToolBar(appPane, space, editToolBar);
        BorderPane borderPane = (BorderPane)gui.getAppPane();
        borderPane.setTop(toolBar);
        
    }
    
    
    @Override
    public void reloadWorkspace() {
        
        // ENABLE EDIT TOOLBAR
        enableToolBarFunctions();
        
        // SET MAP PANE DIMENSSIONS
        mapPane.setMaxSize(dataManager.getMapWidth(), dataManager.getMapHeight());
        mapPane.setMinSize(dataManager.getMapWidth(), dataManager.getMapHeight());
            
        tableView.setItems(dataManager.getSubRegionList());
        app.getGUI().getAppPane().setCenter(pane);
        
        //SETTING THE HEIGHT OF THE PARENT STACK PANE
        parentMapPane.setPrefWidth(app.getGUI().getAppPane().getWidth() * pane.getDividers().get(0).getPosition());
        
        // RESETTING SCALING PROPERTIES
        setZoomProperties();

        // ADDING ALL THE POYGONNS INTO THE MAP PANE
        for(SubRegion sub: dataManager.getSubRegionList())
            for(Polygon pol: sub.getPolygons()){
                polygonGroup.getChildren().add(pol);
                polToSubHashMap.put(pol, sub);
            }  
        mapPane.getChildren().add(polygonGroup);
        
        //UPDATING THE POSITION OF THE MAP
        polygonGroup.setLayoutX(dataManager.getMapDisplacements()[0]);
        polygonGroup.setLayoutY(dataManager.getMapDisplacements()[1]);
        
        //RELOADING SUBREGION COLORS
        reloadSubregionColors();
        
        dataManager.changeBorderWidth(dataManager.getBorderWidth());
        
        // ADDING ALL THE IMAGES
        addImages();
        dataManager.changeMapPaneBackgroundColorByString(dataManager.getBackgroundColorString());
        dataManager.changeBorderColorByString(dataManager.getBorderColorString());
        
        // RESET ALL RESETABLE HANDLERS
        resetMapContentHandlers();
        
        // SET COLOR PICKERS WITH THE RIGHT INITIAL VALUES
        backgroundColorPicker.setValue(dataManager.getBackgroundColor());
        borderColorPicker.setValue(dataManager.getBorderColor());
        
        // BIND ALL BORDERS
        bindAllBorders();
        
        // SET BORDER SLIDER VALUE
        borderWidthSlider.setValue(dataManager.getBorderWidth());
    }
    
    public void bindAllBorders(){
        for(SubRegion sub: dataManager.getSubRegionList())
            for(Polygon pol: sub.getPolygons()){
                pol.strokeWidthProperty().unbind();
                pol.strokeWidthProperty().bind(borderWidthSlider.valueProperty().divide(polygonGroup.getScaleX()));
            }
    }

    private void resetMapContentHandlers(){
        EditorController controller = new EditorController(app);
        
        for(ImageView currentImage: images)
            setImageHandler(currentImage);
        
        
        for(SubRegion sub: dataManager.getSubRegionList()){
            for(Polygon pol: sub.getPolygons()){
                pol.setOnMouseClicked(e -> {
                    if(e.getClickCount() == 2){
                        controller.viewAndEditSubregion();
                        tableView.getColumns().get(0).setVisible(false);
                        tableView.getColumns().get(0).setVisible(true);
                        tableView.getColumns().get(1).setVisible(false);
                        tableView.getColumns().get(1).setVisible(true);
                        tableView.getColumns().get(2).setVisible(false);
                        tableView.getColumns().get(2).setVisible(true);
                        tableView.requestFocus();
                        dataManager.setSubRegionToSelected(tableView.getSelectionModel().getSelectedItem());
                    }
                    else
                        pol.requestFocus();
                    
                    refreshImages();
                });
                
                pol.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue)->{
                    SubRegion currentSubRegion = polToSubHashMap.get(pol);
                    if(newValue){
                        dataManager.setSubRegionToSelected(currentSubRegion);
                        tableView.getSelectionModel().select(sub);
                    }
                    else if(!isSubRegionSelected(currentSubRegion)){
                        currentSubRegion.changePolygonAndBorderColor(
                                currentSubRegion.getColor(), 
                                dataManager.getBorderColor()
                        );
                        
                    }
                    
                });
            }
        }
        
    }
    
    private void setImageHandler(ImageView image){
        DropShadow ds = new DropShadow(20, Color.YELLOW );
        
        image.setOnMouseDragged(e -> {
                double xDis = (e.getX() - (image.getImage().getWidth()/2)) - image.getX();
                double yDis = (e.getY() - (image.getImage().getHeight()/2)) - image.getY();
                
                image.setX(e.getX() - (image.getImage().getWidth()/2));
                image.setY(e.getY() - (image.getImage().getHeight()/2));
                currentIndex = images.indexOf(image);
                
                updateImageDisplacement(xDis, yDis);
                app.getGUI().updateToolbarControls(false);
                
        });
        
        image.setOnMouseClicked(e -> {
            currentImage = image;
            refreshImages();
            image.setEffect(ds);
        });
    }
    
    public void refreshImages(){
        for(ImageView image: images)
            image.setEffect(null);
    }
    
    private boolean isSubRegionSelected(SubRegion sub){
        for(Polygon pol: sub.getPolygons())
            if(pol.isFocused())
                return true;
        
        return false;
    }
    
    private void updateImageDisplacement(double x, double y){
        dataManager.getImageDisplacements().get(currentIndex)[0] += x;
        dataManager.getImageDisplacements().get(currentIndex)[1] += y;
    }
    
    private void setZoomProperties(){
        if(dataManager.getZoomScale() > zoomSlider.getMax()){
            setSliderBinding();
            zoomSlider.setValue(dataManager.getZoomScale());
        }
        else{
            zoomSlider.setValue(dataManager.getZoomScale());
            setSliderBinding();
        }
    }
    
    private void setSliderBinding(){
        double mapArea = dataManager.getMapArea();
        
        if(mapArea < 0.1)
            zoomSlider.setMax(2600);
        else if(mapArea < 1)
            zoomSlider.setMax(1500);
        else if(mapArea < 75)
            zoomSlider.setMax(200);
        else if(mapArea < 75)
            zoomSlider.setMax(200);
        else if(mapArea < 250)
            zoomSlider.setMax(100);
        else if(mapArea < 1000)
            zoomSlider.setMax(50);
        else 
            zoomSlider.setMax(10);
    }
    
    public void reloadSubregionColors(){
        for(SubRegion sub: dataManager.getSubRegionList())
            sub.changePolygonAndBorderColor(sub.getColor(), dataManager.getBorderColor());
    }
    
    public void reloadTableItems(){
        tableView.setItems(dataManager.getSubRegionList());
    }
    
        
    public void clearWorkspace() {
        mapPane.getChildren().clear();
        polygonGroup.getChildren().clear();
        images.clear();
        
    }

    @Override
    public void initStyle() {
        backgroundColorPicker.getStyleClass().add(StartConstants.COLOR_PICKER_STYLE);
        borderColorPicker.getStyleClass().add(StartConstants.COLOR_PICKER_STYLE);
        mapPane.getStyleClass().add(StartConstants.MAP_PANE_STYLE);
        parentMapPane.getStyleClass().add(StartConstants.MAP_PARENT_PANE_STYLE);
        toolBar.getStyleClass().add(StartConstants.CLASS_BORDERED_PANE);
        zoomingLabel.getStyleClass().add(StartConstants.WHITE_LABEL_STYLING);
        borderThicknessLabel.getStyleClass().add(StartConstants.WHITE_LABEL_STYLING);
        boderColorLabel.getStyleClass().add(StartConstants.WHITE_LABEL_STYLING);
        backgroundColorLabel.getStyleClass().add(StartConstants.WHITE_LABEL_STYLING);
        
        // STYLING BUTTONS
        for(Node b: appPane.getChildren())
            b.getStyleClass().add(StartConstants.BACKGROUND_STYLING_BUTTON);
        audioButton.getStyleClass().add(StartConstants.BACKGROUND_STYLING_BUTTON);
        changeColorButton.getStyleClass().add(StartConstants.BACKGROUND_STYLING_BUTTON);
        changeNameButton.getStyleClass().add(StartConstants.BACKGROUND_STYLING_BUTTON);
        reassignColorButton.getStyleClass().add(StartConstants.BACKGROUND_STYLING_BUTTON);
        removeButton.getStyleClass().add(StartConstants.BACKGROUND_STYLING_BUTTON);
        changeDimensionsButton.getStyleClass().add(StartConstants.BACKGROUND_STYLING_BUTTON);
        addImageButton.getStyleClass().add(StartConstants.BACKGROUND_STYLING_BUTTON);
        
    }
    
    private void addImages(){
        for(int i = 0; i < dataManager.getImagePaths().size(); i++){
            Image currentImage = new Image(dataManager.getImagePaths().get(i));
            ImageView imageView = new ImageView(currentImage);
            mapPane.getChildren().add(imageView);
            images.add(imageView);
            imageView.setLayoutX(imageView.getLayoutX() + dataManager.getImageDisplacements().get(i)[0]);
            imageView.setLayoutY(imageView.getLayoutY() + dataManager.getImageDisplacements().get(i)[1]);
        }
    }
    
    public void enableToolBarFunctions(){
        
        // BUTTONS
        audioButton.setDisable(false);
        changeColorButton.setDisable(false);
        changeNameButton.setDisable(false);
        reassignColorButton.setDisable(false);
        changeDimensionsButton.setDisable(false);
        addImageButton.setDisable(false);
        removeButton.setDisable(false);
        
        // SLIDERS
        borderWidthSlider.setDisable(false);
        zoomSlider.setDisable(false);
        
        // COLOR PICKER
        backgroundColorPicker.setDisable(false);
        borderColorPicker.setDisable(false);
    }

    
    public void setProgressBar() {

    }

    public Group getPolygonGroup() {
        return polygonGroup;
    }

    public ColorPicker getBackgroundColorPicker() {
        return backgroundColorPicker;
    }

    public ColorPicker getBorderColorPicker() {
        return borderColorPicker;
    }
    
    public void addImage(ImageView image){
        mapPane.getChildren().add(image);
        images.add(image);
        setImageHandler(image);
    }

    public ImageView getCurrentImage() {
        return currentImage;
    }

    public ArrayList<ImageView> getImages() {
        return images;
    }

    public AudioManager getAudioManager() {
        return audioManager;
    }

    public void setIsMusicPlaying(boolean isMusicPlaying) {
        this.isMusicPlaying = isMusicPlaying;
        updateMusicPlaying();
    }

    public void updateMusicPlaying(){
        PropertiesManager prop = PropertiesManager.getPropertiesManager();
        
        if(isMusicPlaying){
                audioButton.setGraphic(new ImageView(new Image("file:./images/"+prop.getProperty(PropertyType.PLAY_AUDIO_ICON))));
                audioButton.setTooltip(new Tooltip(prop.getProperty(PropertyType.PLAY_SONG_TOOLTIP)));
                isMusicPlaying = false;
            }else{
                audioButton.setGraphic(new ImageView(new Image("file:./images/"+prop.getProperty(PropertyType.STOP_AUDIO_ICON))));
                audioButton.setTooltip(new Tooltip(prop.getProperty(PropertyType.STOP_SONG_TOOLTIP)));
                isMusicPlaying = true;
            }
    }

    
}
