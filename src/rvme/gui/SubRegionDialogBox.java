/*
 * This window will be used to view the information of a sub gerion as well as
 * editing the subregion once the user double clicked on the appropiate
 * area, once clicked 
 */
package rvme.gui;

import java.io.File;
import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import rvme.data.SubRegion;
import saf.AppTemplate;
import rvme.PropertyType;
import rvme.StartConstants;
import rvme.data.DataManager;

/**
 *
 * @author Edwin Alvarez
 */
public class SubRegionDialogBox{
    
    private static final String FILE_PROTOCOL = "file:";
    private static final String NO_IMAGE_DIR = "file:./images/noImage.png";
    
    // LABELS
    Label leaderNameLabel;
    Label nameLabel;
    Label capitalLabel;
    
    // TEXTFIELDS
    TextField nameTextField;
    TextField capitalTextField;
    TextField leaderTextField;
    
    // BUTTONS 
    Button nextButton;
    Button prevButton;
    Button okButton;
    
    // DIALOG PANE
    GridPane pane;
    
    // TOOLBAR FOR THE PREV AND NEXT BUTTONS
    ToolBar prevNextToolBar;
    
    // TOOLBAR FOR THE IMAGES OR NODES
    ToolBar imageToolBar;
    
    //IMAGES
    Image leaderImage;
    Image flagImage;
    
    //VBOX FOR THE PARENT PANE
    VBox root;
    
    // THIS WILL BE USED AS THE SPACE BETWEEN THE NEXT AND PREV BUTTONS
    Rectangle buttonSpace;
    Rectangle imageSpace;
    
    // THE APP TEMPLATE
    AppTemplate app;
    
    // CURRENT SUBREGION
    SubRegion currentSubRegion;
    
    // THE CURRENT WINDOW
    Stage window;
    
    // SCENE
    Scene scene;
    
    // DATA MANAGER
    DataManager dataManager;
    
    // UNIVERSAL INDEX
    int currentSubIndex;
    
    // WAS SOMETHING CHANGED?
    boolean somethingChanged = false;
    
    public SubRegionDialogBox(AppTemplate initApp){
        app = initApp;
        dataManager = (DataManager)app.getDataComponent();
        
        initWindow();
        initHandlers();
        initListeners();
        initStyleSheet();
        initStyle();
        
    }
    
    public void initWindow(){
        window = new Stage();
        window.initModality(Modality.WINDOW_MODAL);
        PropertiesManager pro = PropertiesManager.getPropertiesManager();
        
        root = new VBox();
        pane = new GridPane();
        pane.setPadding(new Insets(10, 20, 10, 20));
        
        initPrevAndNextBar();
        window.setResizable(false);
        
        //SETTING UP THE LABELS
        nameLabel = new Label(pro.getProperty(PropertyType.REGION_NAME_LABEL));
        capitalLabel = new Label(pro.getProperty(PropertyType.CAPITAL_NAME_LABEL));
        leaderNameLabel = new Label(pro.getProperty(PropertyType.LEADER_NAME_LABEL));
        
        //SETTING UP TEXTFIELDS
        nameTextField = new TextField();
        capitalTextField = new TextField();
        leaderTextField = new TextField();
        
        // BUTTON
        okButton = new Button(pro.getProperty(PropertyType.OK));
        
        
        pane.setHgap(10);
        pane.setVgap(8);
        pane.add(nameLabel, 0, 0);
        pane.add(nameTextField, 1, 0);
        pane.add(capitalLabel, 0, 1);
        pane.add(capitalTextField, 1, 1);
        pane.add(leaderNameLabel, 0, 2);
        pane.add(leaderTextField, 1, 2);
        pane.add(okButton, 1, 3);
        GridPane.setHalignment(okButton, HPos.RIGHT);
 
        root.getChildren().addAll(prevNextToolBar, pane);
        
        scene = new Scene(root, 315, 290);
        window.setScene(scene);
        window.setTitle(pro.getProperty(PropertyType.SUBREGION_DIALOG_TITLE));
        
        
    }
    
    public void showAndEditSubregion(Stage parentWindow, SubRegion subRegion){
        window.initOwner(parentWindow);
        currentSubRegion = subRegion;
        reloadWindow();
        window.showAndWait();
        
    }
    
    private void reloadWindow(){
        nameTextField.setText(currentSubRegion.getName());
        capitalTextField.setText(currentSubRegion.getCapital());
        leaderTextField.setText(currentSubRegion.getLeader());
        initImageSection();
        initToolBarStyle();
    }
    
    private void initPrevAndNextBar(){
        Pane containerPane = new Pane();
        Pane space = new Pane();
        HBox.setHgrow(space, Priority.SOMETIMES);
        
        PropertiesManager pro = PropertiesManager.getPropertiesManager();
        
        prevButton = app.getGUI().initChildButton(containerPane, PropertyType.PREV_ICON.toString(), PropertyType.MOVE_LEFT_TOOLTIP.toString(), false);
        nextButton = app.getGUI().initChildButton(containerPane, PropertyType.NEXT_ICON.toString(), PropertyType.MOVE_RIGHT_TOOLTIP.toString(), false);
        prevNextToolBar = new ToolBar(containerPane.getChildren().get(0), space, containerPane.getChildren().get(1));
    }
    
    private void initImageSection(){
        //SETTING IMAGES
        lookForImages();
        
        // SETTING THE LAYOUT
        Pane space = new Pane();
        HBox.setHgrow(space, Priority.SOMETIMES);
        
        //SETTING IMAGES
        ImageView flagView = new ImageView(flagImage);
        flagView.setFitHeight(80);
        flagView.setFitWidth(80);
        flagView.setPreserveRatio(true);
        
        ImageView leaderView = new ImageView(leaderImage);
        leaderView.setFitHeight(80);
        leaderView.setFitWidth(80);
        leaderView.setPreserveRatio(true);
        

        
        //ADDING CHILDREN
        imageToolBar = new ToolBar(flagView, space, leaderView);
        root.getChildren().add(imageToolBar);
        
    }
    
    private void editSubregion(){
        currentSubRegion.setName(nameTextField.getText());
        currentSubRegion.setLeader(leaderTextField.getText());
        currentSubRegion.setCapital(capitalTextField.getText());
    }
    
    private void initStyleSheet() {
	// SELECT THE STYLESHEET
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String stylesheet = props.getProperty(PropertyType.WORKSPACE_PATH_CSS);
	scene.getStylesheets().add(stylesheet);
        
    }
    
    private void initToolBarStyle(){
        imageToolBar.getStyleClass().add(StartConstants.BACKGROUND_STYLING);
    }
    
    private void initStyle(){
        root.getStyleClass().add(StartConstants.BACKGROUND_STYLING);
        prevNextToolBar.getStyleClass().add(StartConstants.BACKGROUND_STYLING);
        nameLabel.getStyleClass().add(StartConstants.WHITE_LABEL_STYLING);
        capitalLabel.getStyleClass().add(StartConstants.WHITE_LABEL_STYLING);
        leaderNameLabel.getStyleClass().add(StartConstants.WHITE_LABEL_STYLING);
        nextButton.getStyleClass().add(StartConstants.BACKGROUND_STYLING_BUTTON);
        prevButton.getStyleClass().add(StartConstants.BACKGROUND_STYLING_BUTTON);
        
    }

    private void initHandlers() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        
        okButton.setOnAction(e -> {
            editSubregion();
            window.close();
            
            // SELECT THE CURRENT ITEM
            workspace.getTable().getSelectionModel().select(currentSubRegion);
            workspace.reloadSubregionColors();
            dataManager.setSubRegionToSelected(currentSubRegion);
            if(somethingChanged)
                app.getGUI().updateToolbarControls(false);
        });
        
        nextButton.setOnAction(e ->{
            selectNextSubRegion();
        });
        
        prevButton.setOnAction(e -> {
            selectPrevSubRegion();
        });
    }
    
    private void initListeners() {
        nameTextField.textProperty().addListener(e -> {
            somethingChanged = true;
        });
        
        capitalTextField.textProperty().addListener(e -> {
            somethingChanged = true;
        });
        
        leaderTextField.textProperty().addListener(e -> {
            somethingChanged = true;
        });
    }

    private void lookForImages() {
       // FOR NOW WE WILL USE DUMMY DATA IMAGES, TO BE DELETED LATER IN THE IMPLEMENTATION
       File leaderImageDir = new File(dataManager.getParentDirectory() + "/" + currentSubRegion.getLeader() + ".png");
       File flagImageDir = new File(dataManager.getParentDirectory() + "/" + currentSubRegion.getName() + " Flag.png");
       
       if(leaderImageDir.exists())
           leaderImage = new Image(FILE_PROTOCOL + leaderImageDir.getPath());
       else
           leaderImage = new Image(NO_IMAGE_DIR);
       
       if(flagImageDir.exists())
           flagImage = new Image(FILE_PROTOCOL + flagImageDir.getPath());
       else
           flagImage = new Image(NO_IMAGE_DIR);
    }

    private void selectNextSubRegion() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        currentSubIndex = workspace.getTable().getItems().indexOf(currentSubRegion);
        int bound = workspace.getTable().getItems().size();
        
        if(currentSubIndex < bound - 1){
            root.getChildren().remove(imageToolBar);    // gonna reset this
            
            currentSubRegion = workspace.getTable().getItems().get(currentSubIndex + 1);
            reloadWindow();
            somethingChanged = false;
        }
        
    }

    private void selectPrevSubRegion() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        currentSubIndex = workspace.getTable().getItems().indexOf(currentSubRegion);
        
        if(currentSubIndex > 0){
            root.getChildren().remove(imageToolBar);    // gonna reset this
            
            currentSubRegion = workspace.getTable().getItems().get(currentSubIndex - 1);
            reloadWindow();
            somethingChanged = false;
        }
    }

    
    
    
}
