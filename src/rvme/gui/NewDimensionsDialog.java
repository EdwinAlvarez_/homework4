/**
 * This class opens a dialog and asks the user for nee dimensions for the map.
 */
package rvme.gui;

import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import rvme.PropertyType;
import rvme.StartConstants;
import saf.AppTemplate;

/**
 *
 * @author Edwin Alvarez
 * @version 1.0
 */
public class NewDimensionsDialog {
    // APP TEMPLATE
    AppTemplate app;
    
    // WINDOW
    Stage window;
    
    // SCENE
    Scene scene;
    
    // PROPERTIES MANAGER
    PropertiesManager pro;
    
    //DIALOG CONTENT
    Label newWidthLabel;
    Label newHeightLabel;
    TextField newWidthTextField;
    TextField newHeightTextField;
    Button continueButton;
    GridPane pane;
    
    // STRING VALUES
    String[] values;
    
    public NewDimensionsDialog(AppTemplate initApp){
        app = initApp;
        
        initWindow();
        initHandlers();
        initStyleSheet();
        initStyle();
    }

    private void initWindow() {
       window = new Stage();
       window.initModality(Modality.WINDOW_MODAL);
       window.setResizable(false);
       
       // INIT VALUES
       values = new String[2];
       
       //INIT PROPERTIES MANANGER
       pro = PropertiesManager.getPropertiesManager();
       
       //INIT CONTENT
       pane = new GridPane();
       newWidthLabel = new Label(pro.getProperty(PropertyType.NEW_WIDTH_LABEL));
       newHeightLabel = new Label(pro.getProperty(PropertyType.NEW_HEIGHT_LABEL));
       newWidthTextField = new TextField();
       newHeightTextField = new TextField();
       continueButton = new Button(pro.getProperty(PropertyType.BUTTON_CONTINUE));
       
       //INIT PANE
       pane.setVgap(8);
       pane.setHgap(10);
       pane.add(newWidthLabel, 0, 0);
       pane.add(newWidthTextField, 1, 0);
       pane.add(newHeightLabel, 0, 1);
       pane.add(newHeightTextField, 1, 1);
       pane.add(continueButton, 1, 2);
       GridPane.setHalignment(continueButton, HPos.RIGHT);
       pane.setAlignment(Pos.CENTER);
       
       scene = new Scene(pane,350,200);
       window.setScene(scene);
       window.setTitle(pro.getProperty(PropertyType.NEW_DIMESIONS_TITLE));
       
    }

    private void initStyleSheet() {
        // SELECT THE STYLESHEET
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String stylesheet = props.getProperty(PropertyType.WORKSPACE_PATH_CSS);
	scene.getStylesheets().add(stylesheet);
    }

    private void initStyle() {
        pane.getStyleClass().add(StartConstants.CLASS_BORDERED_PANE);
        newWidthLabel.getStyleClass().add(StartConstants.WHITE_LABEL_STYLING);
        newHeightLabel.getStyleClass().add(StartConstants.WHITE_LABEL_STYLING);
    }
    
    public String[] getDimensionsAndShow(){
        window.initOwner(app.getGUI().getWindow());
        window.showAndWait();
        
        return values;
    }
    
    public void getValues(){
        values[0] = newWidthTextField.getText();
        values[1] = newHeightTextField.getText();
    }

    private void initHandlers() {
        continueButton.setOnAction(e -> {
            getValues();
            window.close();
        });
    }
}
