/*
 * This class will constains methods that will perform the actions when controllers
 * are being activated in the workspace such as the edit toolbar.
 */
package rvme.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javax.sound.sampled.UnsupportedAudioFileException;
import properties_manager.PropertiesManager;
import rvme.PropertyType;
import rvme.data.DataManager;
import rvme.data.SubRegion;
import rvme.file.FileManager;
import rvme.gui.NewDimensionsDialog;
import rvme.gui.NewMapDialog;
import rvme.gui.NewNameDialog;
import rvme.gui.SubRegionDialogBox;
import rvme.gui.Workspace;
import saf.AppTemplate;
import static saf.settings.AppPropertyType.LOAD_ERROR_MESSAGE;
import static saf.settings.AppPropertyType.LOAD_ERROR_TITLE;
import saf.ui.AppMessageDialogSingleton;

/**
 *
 * @author Edwin Alvarez
 */
public class EditorController {
    static String FORWARD_SLASH = "/";
    static String PNG = ".png";
    static String RVM = ".rvm";
    static String FILE_PROTOCOL = "file:";
    static String ANTHEM_PROTOCOL_MIDI = " National Anthem.mid";
    static String ANTHEM_PROTOCOL_WAVE = " National Anthem.wav";
    
    // APP TEMPLATE
    AppTemplate app;
    
    // STRING CURRENT MUSIC FILE
    String anthemDir;
    
    
    public EditorController(AppTemplate initApp){
        app = initApp;
    }
    
    public void viewAndEditSubregion(){
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        
        // SUBREGION DIALOG BOX
        SubRegionDialogBox subRegionDialogBox = new SubRegionDialogBox(app);

        SubRegion currentSubregion = workspace.getTable().getSelectionModel().getSelectedItem();
        if(currentSubregion != null){
            subRegionDialogBox.showAndEditSubregion(app.getGUI().getWindow(), currentSubregion);
            workspace.reloadTableItems();
        }
    }
    
    public void addImage(){
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        DataManager dataManager = (DataManager)app.getDataComponent();
        FileChooser fileChooser = new FileChooser();
        
        // GET IMAGE PATH
        String imageAbsolutePath = fileChooser.showOpenDialog(app.getGUI().getWindow()).getAbsolutePath();
        
        if(imageAbsolutePath.substring(imageAbsolutePath.length() - 4, imageAbsolutePath.length()).equals(PNG)){
            
            // GET RELATIVE PATH
            Path currentDirectory = Paths.get(".").toAbsolutePath().normalize();
            Path imageDirectory = Paths.get(imageAbsolutePath);
            String imageRelativeDirectory = currentDirectory.relativize(imageDirectory).toString();
        
            // ADD IMAGE TO DATA MANAGER
            dataManager.getImagePaths().add(FILE_PROTOCOL + imageRelativeDirectory);
            dataManager.getImageDisplacements().add(new Double[]{0.0, 0.0});
            Image image = new Image(FILE_PROTOCOL + imageRelativeDirectory);
            ImageView imageView = new ImageView(image);
            workspace.addImage(imageView);
        
            app.getGUI().updateToolbarControls(false);
        }
    }
    
    public void removeImage(){
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        DataManager dataManager = (DataManager)app.getDataComponent();
        
        ImageView image = workspace.getCurrentImage();
        int imageIndex = workspace.getImages().indexOf(image);
        
        if(image != null && image.getEffect() != null){
            workspace.getMapPane().getChildren().remove(image);
            dataManager.getImagePaths().remove(imageIndex);
            dataManager.getImageDisplacements().remove(imageIndex);
            workspace.getImages().remove(imageIndex);
            
            app.getGUI().updateToolbarControls(false);
        }
    }

    
    public void changeMapDiemsions(){
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        DataManager dataManager = (DataManager)app.getDataComponent();
        
        //NEW DIMENSIONS DIALOG
        NewDimensionsDialog newDimensionsDialog = new NewDimensionsDialog(app);
        String[] values = newDimensionsDialog.getDimensionsAndShow();
        try{
            int newWidth = Integer.parseInt(values[0]);
            int newHeight = Integer.parseInt(values[1]);
            
            // UPDATE DATA MANAGER
            dataManager.setMapWidth(newWidth);
            dataManager.setMapHeight(newHeight);
            
            // CHANGE DIMMENIONS IN THE GUI
            workspace.getMapPane().setMaxSize(newWidth, newHeight);
            workspace.getMapPane().setMinSize(newWidth, newHeight);
            
            app.getGUI().updateToolbarControls(false);
            
        }catch(NumberFormatException e){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    PropertiesManager props = PropertiesManager.getPropertiesManager();
	    dialog.show(props.getProperty(PropertyType.NEW_DIMESIONS_ERROR_TITLE), props.getProperty(PropertyType.NEW_DIMESIONS_ERROR_MESSAGE));
        }
    }
    
    public void reassignColors(){
        DataManager dataManager = (DataManager)app.getDataComponent();
        ArrayList<Integer> addedColors = new ArrayList();
        
        // EXTRA SECURITY
        addedColors.add(0);
        addedColors.add(255);
        addedColors.add(256);
        
        int newColor;
        
        for(SubRegion sub: dataManager.getSubRegionList()){
            do{
                newColor = (int)((Math.random() * 254) + 1);
            }while(contains(addedColors, newColor));
            
            sub.setColor(newColor, newColor, newColor);
            addedColors.add(newColor);
        }
    }
    
    /**
     * nlog(n) complexity for searching if an integer is inside of an array.
     * @param array
     * @param key the integer to look for
     * @return if integer was found
     */
    
    public boolean contains(final ArrayList<Integer> array, final int key) {  
        Collections.sort(array);  
        return Collections.binarySearch(array, key) >= 0;  
    }
    
    public void changeRegionName() throws IOException{
        DataManager dataManager = (DataManager)app.getDataComponent();
        NewNameDialog nameDialog = new NewNameDialog(app);
        String newRegionName = nameDialog.getNameAndShow();
        if(newRegionName != null){
            
            // GET THE NEW PARENT DIRECTORY
            String newParentDir = createNewDir(dataManager.getParentDirectory(), newRegionName, dataManager);
            
            // CHANGE FILE NAMES
            changeFileNames(dataManager.getParentDirectory(),newRegionName, dataManager, newParentDir);
            
            // FINALLY SET THE NEW NAME TO DATA MANAGER
            dataManager.setRegionName(newRegionName);
            
            // UPDATE THIS IMFORMATION ON THE JSON FILE TO AVOID MISTAKES
            DataManager dummyDataManager = new DataManager(null);
            FileManager fileManager = (FileManager)app.getFileComponent();
            fileManager.loadData(dummyDataManager, FileManager.currentWorkFile);
            dummyDataManager.setParentDirectory(dataManager.getParentDirectory());
            dummyDataManager.setRegionName(dataManager.getRegionName());
            fileManager.saveData(dummyDataManager, FileManager.currentWorkFile);
            
            app.getGUI().updateToolbarControls(false);
            
        }
    }

    private String createNewDir(String currentParentDir, String newRegionName, DataManager data) {
        int concatDirValue = currentParentDir.length() - data.getRegionName().length();
        return currentParentDir.substring(0, concatDirValue) + newRegionName;
    }

    private void changeFileNames(String oldParentDir, String newName, DataManager dataManager, String newParentDir) {
        
        dataManager.setParentDirectory(newParentDir);
        
        // CHANGE THE PARENT DIRECTORY
        File oldDir = new File(oldParentDir);
        oldDir.renameTo(new File(dataManager.getParentDirectory()));
        
        // NOW CHANGE THE FILE NAMES
        String oldPngDir, newPngDir, newRVMDir, oldRVMDir;
        oldPngDir = dataManager.getParentDirectory() + FORWARD_SLASH + dataManager.getRegionName() + PNG;
        newPngDir = dataManager.getParentDirectory() + FORWARD_SLASH + newName + PNG;
        oldRVMDir = dataManager.getParentDirectory() + FORWARD_SLASH + dataManager.getRegionName() + RVM;
        newRVMDir = dataManager.getParentDirectory() + FORWARD_SLASH + newName + RVM;
        
        File imageDir = new File(oldPngDir);
        if(imageDir.exists())
            imageDir.renameTo(new File(newPngDir));
        
        File rvmDir = new File(oldRVMDir);
        if(rvmDir.exists())
            rvmDir.renameTo(new File(newRVMDir));
    }
    
    public void changeMapColors(){
        DataManager dataManager = (DataManager)app.getDataComponent();
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        Color oldBackGroundColor = dataManager.getBackgroundColor();
        Color oldBorderColor = dataManager.getBorderColor();
        
        dataManager.changeMapPaneBackgroundColor(
                workspace.getBackgroundColorPicker().getValue()
        );
        
        dataManager.changeMapPaneBorderColor(
                workspace.getBorderColorPicker().getValue()
        );
        
        if(!oldBackGroundColor.equals(dataManager.getBackgroundColor()) ||
                !oldBorderColor.equals(dataManager.getBorderColor()))
            app.getGUI().updateToolbarControls(false);
    }
    
    public void playAudio(){
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        DataManager dataManager = (DataManager)app.getDataComponent();
        String midiFile = dataManager.getParentDirectory() + FORWARD_SLASH + dataManager.getRegionName() + ANTHEM_PROTOCOL_MIDI;
        String waveFile = dataManager.getParentDirectory() + FORWARD_SLASH + dataManager.getRegionName() + ANTHEM_PROTOCOL_WAVE;
        
        if(new File(midiFile).exists())
            anthemDir = midiFile;
        else if(new File(waveFile).exists())
            anthemDir = waveFile;
        
        if(anthemDir != null){
            try{
                workspace.getAudioManager().loadAudio(dataManager.getRegionName(), anthemDir);
                workspace.getAudioManager().play(dataManager.getRegionName(), true);
                workspace.setIsMusicPlaying(true);
                workspace.updateMusicPlaying();
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        else{
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    PropertiesManager props = PropertiesManager.getPropertiesManager();
	    dialog.show(props.getProperty(PropertyType.ANTHEM_NOT_FOUND_TITLE), props.getProperty(PropertyType.ANTHEM_NOT_FOUND_MESSAGE));
        }
    }
    
    public void stopAudio(){
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        DataManager dataManager = (DataManager)app.getDataComponent();
        workspace.getAudioManager().stop(dataManager.getRegionName());
        
        workspace.setIsMusicPlaying(false);
        workspace.updateMusicPlaying();
        anthemDir = null;
    }
    
}
