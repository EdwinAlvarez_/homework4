/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SavingLoadingTesting;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import rvme.data.DataManager;
import rvme.file.FileManager;
import rvme.test_bed.TestLoad;
import rvme.test_bed.TestSave;

/**
 *
 * @author Edwin Alvarez
 */
public class SlovakiaTest {
    
    public SlovakiaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of main method, of class TestLoad.
     */
    @Test
    public void savingAndLoading() throws Exception {
        System.out.println("* SlovakiaTest: mapDataCheck()");
        TestSave.setDataManager(new DataManager(null));
        TestSave.setFileManager(new FileManager());
        TestLoad.setDataManager(new DataManager(null));
        TestLoad.setFileManager(new FileManager());
        TestSave.saveSlovakiaData();
        TestLoad.loadSlovakiaData();
        
        assertEquals(TestSave.parentDirectory, TestLoad.dataManager.getParentDirectory());
        assertEquals(TestSave.regionName, TestLoad.dataManager.getRegionName());
        assertEquals(TestSave.borderWidth, TestLoad.dataManager.getBorderWidth(), 0.001);
        assertEquals(TestSave.backgroundColor, TestLoad.dataManager.getBackgroundColorString());
        assertEquals(TestSave.borderColor, TestLoad.dataManager.getBorderColorString());
        assertEquals(TestSave.coordinatePath, TestLoad.dataManager.getCoordinateFilePath());
        assertEquals(TestSave.mapDisplacementX, TestLoad.dataManager.getMapDisplacements()[0], 0.001);
        assertEquals(TestSave.mapDisplacementY, TestLoad.dataManager.getMapDisplacements()[1], 0.001);
        assertEquals(TestSave.subRegionInfo.size(), TestLoad.dataManager.getNumOfSubregions());
        assertEquals(TestSave.mapWidth, TestLoad.dataManager.getMapWidth(), 0.001);
        assertEquals(TestSave.mapHeight, TestLoad.dataManager.getMapHeight(), 0.001);
        assertEquals(TestSave.zoomScale, TestLoad.dataManager.getZoomScale(), 0.001);
        assertEquals(TestSave.numOfSubRegions, TestLoad.dataManager.getNumOfSubregions());
        
        // COMPARING THE IMAGES PATHS AND THEIR DISPLACEMENTS
        for(int i = 0; i < TestSave.imagesPaths.length; i++){
            assertEquals(TestSave.imagesPaths[i], TestLoad.dataManager.getImagePaths().get(i));
            assertEquals(TestSave.imageDisplacements.get(i)[0], TestLoad.dataManager.getImageDisplacements().get(i)[0], 0.001);
            assertEquals(TestSave.imageDisplacements.get(i)[1], TestLoad.dataManager.getImageDisplacements().get(i)[1], 0.001);
        }
        
        // COMPARING THEIR SUBREGION DATA
        for(int i = 0; i < TestSave.subRegionInfo.size(); i++){
            assertEquals(TestSave.subRegionInfo.get(i)[0], TestLoad.dataManager.getSubRegionList().get(i).getName());
            assertEquals(TestSave.subRegionInfo.get(i)[1], TestLoad.dataManager.getSubRegionList().get(i).getCapital());
            assertEquals(TestSave.subRegionInfo.get(i)[2], TestLoad.dataManager.getSubRegionList().get(i).getLeader());
            assertEquals(TestSave.subregionColors.get(i)[0],converToInteger(TestLoad.dataManager.getSubRegionList().get(i).getColor().getRed() * 256));
            assertEquals(TestSave.subregionColors.get(i)[1], converToInteger(TestLoad.dataManager.getSubRegionList().get(i).getColor().getGreen() * 256));
            assertEquals(TestSave.subregionColors.get(i)[2], converToInteger(TestLoad.dataManager.getSubRegionList().get(i).getColor().getBlue() * 256));
            assertEquals(TestSave.numOfPolygons[i], TestLoad.dataManager.getSubRegionList().get(i).getNumOfPolygons());
            
        }
        
    }
    
    private Integer converToInteger(double x){
        return (int)x;
    }
    
}
